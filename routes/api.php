<?php

use App\Http\Controllers\API\Auth\AuthController;
use App\Http\Controllers\API\Auth\PasswordResetController;
use App\Http\Controllers\API\Dashboard\StoreController;
use App\Http\Controllers\API\Mobile\DealController;
use App\Http\Controllers\API\Mobile\FavouritesController;
use App\Http\Controllers\API\Mobile\NotificationsController;
use App\Http\Controllers\API\Mobile\SearchController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
*/

Route::group([
    'prefix' => 'auth',
], function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::post('register', [AuthController::class, 'register']);

    Route::group(['middleware' => ['auth:sanctum']], function () {
        Route::get('user', [AuthController::class, 'user']);
        Route::post('update', [AuthController::class, 'update']);
        Route::get('register/activate/{token}', [AuthController::class, 'registerActivate']);
    });

    Route::group([
        'prefix' => 'password'
    ], function () {
        Route::post('create', [PasswordResetController::class, 'create']);
        Route::post('reset', [PasswordResetController::class, 'reset']);
    });

    Route::group([
        'middleware' => 'auth:sanctum'
    ], function () {
        Route::get('logout', [AuthController::class, 'logout']);
        Route::get('user', [AuthController::class, 'user']);
        Route::post('update', [AuthController::class, 'update']);
        Route::get('images', [AuthController::class, 'getImages']);
        Route::post('images', [AuthController::class, 'uploadImages']);
        Route::delete('images', [AuthController::class, 'deleteImages']);
        Route::put('images', [AuthController::class, 'setFeaturedImage']);
    });
});

Route::group([
    'prefix' => 'deals',
    'middleware' => 'auth:sanctum'
], function () {

    Route::get('images', [DealController::class, 'getImages']);
    Route::post('images', [DealController::class, 'uploadImages']);
    Route::delete('images', [DealController::class, 'deleteImages']);
    Route::put('images', [DealController::class, 'setFeaturedImage']);
});

Route::apiResource('deals', DealController::class);

Route::group([
    'prefix' => 'stores',
    'middleware' => 'auth:sanctum'
], function () {

    Route::get('images', StoreController::class, 'getImages');
    Route::post('images', [StoreController::class, 'uploadImages'] );
    Route::delete('images', [StoreController::class, 'deleteImages'] );
    Route::put('images', [StoreController::class, 'setFeaturedImage'] );
});

Route::group([
    'prefix' => 'mobile',
    'middleware' => 'auth:sanctum'
], function () {

    Route::get('provinces', [DealController::class, 'allProvinces'] );

    Route::apiResource('stores', StoreController::class);

    Route::group([
        'prefix' => 'deals',
    ], function () {
        Route::get('categories', [DealController::class, 'categories'] );
        Route::post('smash', [DealController::class, 'smash'] );
        Route::post('grab', [DealController::class, 'grab'] );
    });

    Route::apiResource('deals', DealController::class);

    Route::group([
        'prefix' => 'search',
    ], function () {
        Route::post('location', [SearchController::class, 'searchByLocation'] );
        Route::post('categories', [SearchController::class, 'searchByCategories'] );
        Route::post('province', [SearchController::class, 'searchByProvince'] );
        Route::post('text', [SearchController::class, 'searchByText'] );
    });

    Route::group([
        'prefix' => 'favourites',
    ], function () {
        Route::get('/', [FavouritesController::class, 'index'] );
        Route::post('create', [FavouritesController::class, 'create'] );
    });

    Route::group([
        'prefix' => 'notifications',
    ], function () {
        Route::get('/', [NotificationsController::class, 'index'] );
        Route::post('remove', [NotificationsController::class, 'remove'] );
        Route::post('check', [NotificationsController::class, 'check'] );

        Route::group([
            'prefix' => 'push',
        ], function () {
            Route::post('/register', [NotificationsController::class, 'registerPush'] );
            Route::post('remove', [NotificationsController::class, 'sendPush'] );
        });
    });
});

