@extends('layouts/fullLayoutMaster')

@section('title', 'Register Page')

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/pages/authentication.css')) }}">
@endsection

@section('content')
  <div class="auth-wrapper auth-basic px-2">
    <div class="auth-inner my-2">
      <!-- Register Basic -->
      <div class="card mb-0">
        <div class="card-body">
          <a href="#" class="brand-logo">
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 194 249.41" width="90" height="60">
                  <defs>
                      <style>
                          .cls-1 {
                              fill: #2cabb7;
                          }
                      </style>
                  </defs>
                  <g id="Layer_2" data-name="Layer 2">
                      <g id="Layer_1-2" data-name="Layer 1">
                          <path class="cls-1"
                                d="M194.32,40.73c.06-9.2-2.78-12.47-12.24-12.48-9.69,0-25.22,0-43.08,0-12.87-37.69-70.88-37.67-83.69.07l-44.89.05C3.59,28.47.08,31.76.08,38.78Q.11,139.1,0,239.44c0,7.28,3.37,10,10.36,9.95,17.66-.18,35.33-.29,53,0,8.19.14,8.95-4.49,8.9-10.88s-.1-12.18-9.12-11.9c-10.52.32-20.85.12-31.56.25-9.08.11-11.52-2.56-11.46-11.49q.59-78.91,0-157.82C20,48.25,23.27,46.21,31.88,46.28q67.08.51,134.15,0c7.75-.06,10.39,1.93,10.34,10.06q-.47,79.47.07,159c.06,8.12-1.72,11-10.56,11.17-17.66.41-34.11,0-51.82-.13-7-.06-8.07-3.35-8-9.09.16-19.17-1.23-38.51-.14-57.49.52-9.15,2.87-15.59,9.22-21.69,13-12.48,25.42-25.52,38.23-38.18,5.07-5,4.72-9.27,0-14.45-6.06-6.62-11.07-6-16.9.24-12.82,13.64-25.95,27-39.6,41.15C82,114.37,68.5,103.18,55.16,91.81,49,86.54,42.91,84,36.86,91.57c-5.71,7.14-3.47,12.69,3.4,18,11.55,9,22.3,18.93,34,27.69,7.79,5.84,10.2,12.84,10,22.23-.54,24-.35,48.08-.39,72.12,0,7.08.43,14.33,10,14.39q45.06.31,90.13.08c7.25,0,10.67-3.75,10.28-11.48ZM65.52,28.3c11.81-25.23,51.44-25.25,63.27,0C108.15,28.26,85.47,28.28,65.52,28.3Z" />
                      </g>
                  </g>
              </svg>
          </a>

          <h4 class="card-title mb-1">Your Adventure starts here 🚀</h4>
          <p class="card-text mb-2">Make your app management easy and fun!</p>


          <form class="auth-register-form mt-2" method="POST" action="{{ route('register') }}">
            @csrf
            <div class="mb-1">
              <label for="register-username" class="form-label">Username</label>
              <input type="text" class="form-control @error('name') is-invalid @enderror" id="register-username"
                name="name" placeholder="johndoe" aria-describedby="register-username" tabindex="1" autofocus
                value="{{ old('name') }}" />
              @error('name')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>
            <div class="mb-1">
              <label for="register-email" class="form-label">Email</label>
              <input type="text" class="form-control @error('email') is-invalid @enderror" id="register-email"
                name="email" placeholder="john@example.com" aria-describedby="register-email" tabindex="2"
                value="{{ old('email') }}" />
              @error('email')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>

            <div class="mb-1">
              <label for="register-password" class="form-label">Password</label>

              <div class="input-group input-group-merge form-password-toggle @error('password') is-invalid @enderror">
                <input type="password" class="form-control form-control-merge @error('password') is-invalid @enderror"
                  id="register-password" name="password"
                  placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                  aria-describedby="register-password" tabindex="3" />
                <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
              </div>
              @error('password')
                <span class="invalid-feedback" role="alert">
                  <strong>{{ $message }}</strong>
                </span>
              @enderror
            </div>

            <div class="mb-1">
              <label for="register-password-confirm" class="form-label">Confirm Password</label>

              <div class="input-group input-group-merge form-password-toggle">
                <input type="password" class="form-control form-control-merge" id="register-password-confirm"
                  name="password_confirmation"
                  placeholder="&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;&#xb7;"
                  aria-describedby="register-password" tabindex="3" />
                <span class="input-group-text cursor-pointer"><i data-feather="eye"></i></span>
              </div>
            </div>
            @if (Laravel\Jetstream\Jetstream::hasTermsAndPrivacyPolicyFeature())
              <div class="mb-1">
                <div class="form-check">
                  <input class="form-check-input" type="checkbox" id="terms" name="terms" tabindex="4" />
                  <label class="form-check-label" for="terms">
                    I agree to the <a href="{{ route('terms.show') }}" target="_blank">terms_of_service</a> and
                    <a href="{{ route('policy.show') }}" target="_blank">privacy_policy</a>
                  </label>
                </div>
              </div>
            @endif
            <button type="submit" class="btn btn-primary w-100" tabindex="5">Sign up</button>
          </form>

          <p class="text-center mt-2">
            <span>Already have an account?</span>
            @if (Route::has('login'))
              <a href="{{ route('login') }}">
                <span>Sign in instead</span>
              </a>
            @endif
          </p>

          <div class="divider my-2">
            <div class="divider-text">or</div>
          </div>

          <div class="auth-footer-btn d-flex justify-content-center">
            <a href="#" class="btn btn-facebook">
              <i data-feather="facebook"></i>
            </a>
            <a href="#" class="btn btn-twitter white">
              <i data-feather="twitter"></i>
            </a>
            <a href="#" class="btn btn-google">
              <i data-feather="mail"></i>
            </a>
            <a href="#" class="btn btn-github">
              <i data-feather="github"></i>
            </a>
          </div>
        </div>
      </div>
      <!-- /Register basic -->
    </div>
  </div>
@endsection
