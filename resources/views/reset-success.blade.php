@extends('layouts.main')

@section('content')
    <div style="max-width: 400px; margin: auto; margin-top: 200px">
        <h1 class="h3 mb-3 font-weight-normal">Reset Successful!</h1>
        <p>You may now login. <a href="https://app.itsyopa.com/login">Go To Login</a></p>
    </div>
@endsection