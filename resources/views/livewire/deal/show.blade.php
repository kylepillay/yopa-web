<section>
    @if (session()->has('message'))
        <div class="row">
            <div class="col-12">
                <div class="alert alert-success alert-dismissible fade show">
                    <div class="alert-body">
                        {{ session('message') }}
                    </div>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{ $add ? 'Add Deal' : 'Edit Deal' }}</h4>
                    <a href="{{ url('admin/deals') }}" class="btn btn-outline-primary waves-effect">Back</a>
                </div>
                <div class="card-body">
                    <form class="form form-vertical needs-validation" wire:submit.prevent="save">
                        <div class="row">
                            <div class="row">
                                <div class="col-6">
                                    <div class="mb-1" style="max-width: 200px">
                                       @if($deal->getFirstMedia('images')) <img style="max-width: 100%"
                                                  src="{{ $deal->getFirstMedia('images') ? $deal->getFirstMedia('images')->getUrl() : '#' }}"/>@endif
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="col-form-label" for="first-name">Name</label>
                                        <input wire:model="deal.name" type="text" id="first-name"
                                               class="form-control @error('deal.name') is-invalid @enderror" name="name"
                                               placeholder="Name">
                                        @error('deal.name')
                                        <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="col-form-label" for="photo">Image</label>
                                        <x-media-library-attachment name="photo"/>
                                    </div>

                                    @error('photo')
                                    <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="col-form-label" for="email-id">Handle</label>
                                        <input wire:model="deal.handle" type="handle" id="email-id"
                                               class="form-control @error('deal.handle') is-invalid @enderror"
                                               name="handle" placeholder="Handle">
                                        @error('deal.handle')
                                        <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="mb-1">
                                    <label class="col-form-label" for="password">Description</label>
                                    <input wire:model="deal.description" type="text" id="phone"
                                           class="form-control @error('deal.description') is-invalid @enderror"
                                           name="description"
                                           placeholder="Description">
                                    @error('deal.description')
                                    <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-1">
                                    <label class="col-form-label" for="active">Is Active</label>
                                    <div class="form-check form-switch">
                                        <input wire:model="deal.active" type="checkbox"
                                               class="form-check-input @error('deal.active') is-invalid @enderror"
                                               id="active">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="mb-1">
                                    <label class="col-form-label" for="first-name">Link</label>
                                    <input wire:model="deal.link" type="text" id="link"
                                           class="form-control @error('deal.link') is-invalid @enderror" name="link"
                                           placeholder="Link">
                                    @error('deal.link')
                                    <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="mb-1">
                                    <label class="col-form-label" for="first-name">Price</label>
                                    <input wire:model="deal.price" type="text" id="price"
                                           class="form-control @error('deal.price') is-invalid @enderror" name="price"
                                           placeholder="Price">
                                    @error('deal.price')
                                    <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-1">
                                    <label class="col-form-label" for="first-name">Sale Price</label>
                                    <input wire:model="deal.salePrice" type="text" id="salePrice"
                                           class="form-control @error('deal.salePrice') is-invalid @enderror" name="salePrice"
                                           placeholder="Sale Price">
                                    @error('deal.salePrice')
                                    <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="mb-1">
                                    <label class="col-form-label" for="first-name">Latitude</label>
                                    <input wire:model="deal.latitude" type="text" id="latitude"
                                           class="form-control @error('deal.latitude') is-invalid @enderror"
                                           name="latitude"
                                           placeholder="Latitude">
                                    @error('deal.latitude')
                                    <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-1">
                                    <label class="col-form-label" for="first-name">Longitude</label>
                                    <input wire:model="deal.longitude" type="text" id="longitude"
                                           class="form-control @error('deal.longitude') is-invalid @enderror"
                                           name="longitude"
                                           placeholder="Longitude" data-bts-step="0.5" data-bts-decimals="2">
                                    @error('deal.latitude')
                                    <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="mb-1">
                                    <label class="col-form-label" for="contact-info">Category</label>
                                    <select wire:model="deal.category_id"
                                            class="form-select @error('deal.category_id') is-invalid @enderror" id="category_id">
                                        <option value="" selected>Select a Category</option>
                                        @foreach($categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('deal.category_id')
                                    <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-1">
                                    <label class="col-form-label" for="contact-info">User</label>
                                    <select wire:model="deal.user_id"
                                            class="form-select @error('deal.user_id') is-invalid @enderror" id="user_id">
                                        <option value="" selected>Select a User</option>
                                        @foreach($users as $user)
                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('deal.user_id')
                                    <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="mb-1">
                                    <label class="col-form-label" for="contact-info">Store</label>
                                    <select wire:model="deal.store_id"
                                            class="form-select @error('deal.store_id') is-invalid @enderror" id="store_id">
                                        <option value="" selected>Select a Store</option>
                                        @foreach($stores as $store)
                                            <option value="{{ $store->id }}">{{ $store->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('deal.store_id')
                                    <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-12 pt-2">
                            <button type="submit" class="btn btn-primary me-1 waves-effect waves-float waves-light">
                                Save
                            </button>
                            <button type="reset" class="btn btn-outline-secondary waves-effect">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
