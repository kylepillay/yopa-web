<section>
    @if (session()->has('message'))
        <div class="row">
            <div class="col-12">
                <div class="alert alert-success alert-dismissible fade show">
                    <div class="alert-body">
                        {{ session('message') }}
                    </div>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{ $add ? 'Add User' : 'Edit User' }}</h4>
                    <a href="{{ url('admin/users') }}" class="btn btn-outline-primary waves-effect">Back</a>
                </div>
                <div class="card-body">
                    <form class="form form-vertical needs-validation" wire:submit.prevent="save">
                        <div class="row">
                            <div class="col-6">
                                <div class="mb-1">
                                    <label class="col-form-label" for="first-name">Name</label>
                                    <input wire:model="user.name" type="text" id="first-name"
                                           class="form-control @error('user.name') is-invalid @enderror" name="name"
                                           placeholder="Name">
                                    @error('user.name')
                                    <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-1">
                                    <label class="col-form-label" for="email-id">Email</label>
                                    <input wire:model="user.email" type="email" id="email-id"
                                           class="form-control @error('user.email') is-invalid @enderror"
                                           name="email-id" placeholder="Email">
                                    @error('user.email')
                                    <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-1">
                                    <label class="col-form-label" for="password">Phone</label>
                                    <input wire:model="user.phone" type="text" id="phone"
                                           class="form-control @error('user.phone') is-invalid @enderror" name="phone"
                                           placeholder="Phone">
                                    @error('user.phone')
                                    <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-1">
                                    <label class="col-form-label" for="active">Is Active</label>
                                    <div class="form-check form-switch">
                                        <input wire:model="user.active" type="checkbox"
                                               class="form-check-input @error('user.active') is-invalid @enderror"
                                               id="active">
                                    </div>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-1">
                                    <label class="col-form-label" for="contact-info">Role</label>
                                    <select wire:model="user.role"
                                            class="form-select @error('user.role') is-invalid @enderror" id="role">
                                        <option value="user">User</option>
                                        <option value="admin">Admin</option>
                                        <option value="staff">Staff</option>
                                    </select>
                                    @error('user.role')
                                    <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-1">
                                    <label class="col-form-label" for="password">City</label>
                                    <input wire:model="user.city" type="text" id="city"
                                           class="form-control @error('user.city') is-invalid @enderror" name="city"
                                           placeholder="City">
                                    @error('user.city')
                                    <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-12 pt-2">
                                <button type="submit" class="btn btn-primary me-1 waves-effect waves-float waves-light">
                                    Save
                                </button>
                                <button type="reset" class="btn btn-outline-secondary waves-effect">Reset</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
