<section>
    @if (session()->has('message'))
        <div class="row">
            <div class="col-12">
                <div class="alert alert-success alert-dismissible fade show">
                    <div class="alert-body">
                        {{ session('message') }}
                    </div>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{ $add ? 'Add Category' : 'Edit Category' }}</h4>
                    <a href="{{ url('admin/categories') }}" class="btn btn-outline-primary waves-effect">Back</a>
                </div>
                <div class="card-body">
                    <form class="form form-vertical needs-validation" wire:submit.prevent="save">
                        <div class="row">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="col-form-label" for="first-name">Name</label>
                                        <input wire:model="category.name" type="text" id="category-name"
                                               class="form-control @error('category.name') is-invalid @enderror" name="category.name"
                                               placeholder="Category Name">
                                        @error('category.name')
                                        <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 pt-2">
                            <button type="submit" class="btn btn-primary me-1 waves-effect waves-float waves-light">
                                Save
                            </button>
                            <button type="reset" class="btn btn-outline-secondary waves-effect">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
