<section>
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Stores</h4>
                    <a class="btn btn-outline-primary waves-effect" href="{{ url('/admin/stores/add') }}">Add Store</a>
                </div>
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th>Name</th>
                            <th>Latitude</th>
                            <th>Longitude</th>
                            <th>Status</th>
                            <th>Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($stores as $store)
                            <tr>
                                <td>
                                    @if($store->getFirstMedia('images'))
                                        <img
                                            src="{{ $store->getFirstMedia('images')->getUrl() }}"
                                            class="me-5"
                                            height="20"
                                            width="20"
                                            alt="Angular"
                                            style="border-radius: 100%;"
                                        />
                                    @endif
                                    <span class="fw-bold">{{ $store->name }}</span>
                                </td>
                                <td>{{ $store->latitude }}</td>
                                <td>{{ $store->longitude }}</td>
                                <td><span class="badge rounded-pill {{ $store->active ? 'badge-light-primary' : 'badge-light-danger' }} me-1">{{ $store->active ? 'Active' : 'Inactive' }}</span></td>
                                <td>
                                    <div class="dropdown">
                                        <button type="button" class="btn btn-sm dropdown-toggle hide-arrow py-0"
                                                data-bs-toggle="dropdown">
                                            <i data-feather="more-vertical"></i>
                                        </button>
                                        <div class="dropdown-menu dropdown-menu-end">
                                            <a class="dropdown-item" href="{{ url('admin/stores/'.$store->id) }}">
                                                <i data-feather="edit-2" class="me-50"></i>
                                                <span>Edit</span>
                                            </a>
                                            <a class="dropdown-item" href="#">
                                                <i data-feather="trash" class="me-50"></i>
                                                <span>Delete</span>
                                            </a>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="card px-1 py-1 d-flex flex-sm-column flex-lg-row align-items-center justify-content-between">
                <div>Showing {{ $stores->count() }} items from of a total of {{ $stores->total() }}.</div>
                <div style="display: flex; flex-direction: row; justify-content: center; align-items: center">
                    <span for="contact-info" class="px-1" style="min-width: 100px">Per Page</span>
                    <select wire:model="perPage" class="form-select" style="min-width: 100px">
                        <option value="5">5</option>
                        <option value="10">10</option>
                        <option value="15">15</option>
                        <option value="25">25</option>
                        <option value="50">50</option>
                        <option value="100">100</option>
                    </select>
                </div>
                <div class="pt-1">{{ $stores->links('pagination::bootstrap-4') }}</div>
            </div>
        </div>
    </div>
</section>

