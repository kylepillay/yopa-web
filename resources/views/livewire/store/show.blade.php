<section>
    @if (session()->has('message'))
        <div class="row">
            <div class="col-12">
                <div class="alert alert-success alert-dismissible fade show">
                    <div class="alert-body">
                        {{ session('message') }}
                    </div>
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                </div>
            </div>
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">{{ $add ? 'Add Store' : 'Edit Store' }}</h4>
                    <a href="{{ url('admin/stores') }}" class="btn btn-outline-primary waves-effect">Back</a>
                </div>
                <div class="card-body">
                    <form class="form form-vertical needs-validation" wire:submit.prevent="save">
                        <div class="row">
                            <div class="row">
                                <div class="col-6">
                                    <div class="mb-1" style="max-width: 200px">
                                        @if($store->getFirstMedia('images'))<img style="max-width: 100%"
                                                  src="{{ $store->getFirstMedia('images') ? $store->getFirstMedia('images')->getUrl() : '#' }}"/>@endif
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="col-form-label" for="first-name">Name</label>
                                        <input wire:model="store.name" type="text" id="first-name"
                                               class="form-control @error('store.name') is-invalid @enderror" name="name"
                                               placeholder="Name">
                                        @error('store.name')
                                        <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="col-form-label" for="photo">Image</label>
                                        <x-media-library-attachment name="photo"/>
                                    </div>

                                    @error('photo')
                                    <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="col-form-label" for="email-id">Address</label>
                                        <input wire:model="store.address" type="text" id="address"
                                               class="form-control @error('store.address') is-invalid @enderror"
                                               name="address" placeholder="Address">
                                        @error('store.address')
                                        <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                            {{ $message }}
                                        </div>
                                        @enderror
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="mb-1">
                                    <label class="col-form-label" for="password">Description</label>
                                    <input wire:model="store.description" type="text" id="phone"
                                           class="form-control @error('store.description') is-invalid @enderror"
                                           name="description"
                                           placeholder="Description">
                                    @error('store.description')
                                    <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-1">
                                    <label class="col-form-label" for="active">Is Active</label>
                                    <div class="form-check form-switch">
                                        <input wire:model="store.active" type="checkbox"
                                               class="form-check-input @error('store.active') is-invalid @enderror"
                                               id="active">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="mb-1">
                                    <label class="col-form-label" for="first-name">Latitude</label>
                                    <input wire:model="store.latitude" type="text" id="latitude"
                                           class="form-control @error('store.latitude') is-invalid @enderror"
                                           name="latitude"
                                           placeholder="Latitude">
                                    @error('store.latitude')
                                    <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="mb-1">
                                    <label class="col-form-label" for="first-name">Longitude</label>
                                    <input wire:model="store.longitude" type="text" id="longitude"
                                           class="form-control @error('store.longitude') is-invalid @enderror"
                                           name="longitude"
                                           placeholder="Longitude" data-bts-step="0.5" data-bts-decimals="2">
                                    @error('store.latitude')
                                    <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-6">
                                <div class="mb-1">
                                    <label class="col-form-label" for="contact-info">User</label>
                                    <select wire:model="store.user_id"
                                            class="form-select @error('store.user_id') is-invalid @enderror" id="user_id">
                                        <option value="" selected>Select a User</option>
                                        @foreach($users as $user)
                                            <option value="{{ $user->id }}">{{ $user->name }}</option>
                                        @endforeach
                                    </select>
                                    @error('store.user_id')
                                    <div class="text-danger" style="padding-top: 4px; padding-left: 4px">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                            </div>
                        </div>
                        <div class="col-12 pt-2">
                            <button type="submit" class="btn btn-primary me-1 waves-effect waves-float waves-light">
                                Save
                            </button>
                            <button type="reset" class="btn btn-outline-secondary waves-effect">Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
