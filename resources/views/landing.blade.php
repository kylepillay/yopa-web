@extends('layouts.frontend')

@section('content')
    <div class="hero product-hero is-app-grey is-relative" data-page-theme="teal">
        @include('partials.frontend-navigation')
    <!-- Hero image -->
    <div id="main-hero" class="hero-body" style="padding-top: 80px; padding-bottom: 50px;">
        <div class="container">
            <div class="columns is-vcentered">
                <div class="column is-5 is-offset-1 signup-column has-text-left">
                    <!--Update Pill-->
                    <div class="update-pill">
                        <span class="inner-tag">New Update</span>
                        <span class="inner-text">Check out the newest features</span>
                    </div>

                    <h2 class="subtitle is-5 body-color">
                        Introducing the Yopa Discount App.
                    </h2>
                    <h1 class="title main-title text-bold dark-text is-2">
                        Make everyday Black Friday.
                    </h1>

                    <div class="buttons mt-20">
                        <a href="{{ url('/login') }}" class="button button-cta btn-align is-bold primary-btn">
                            Get Started
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Clients -->
    <div class="hero-foot">
        <div class="container">
            <div class="tabs partner-tabs is-centered">
                <ul>
                    <li>
                        <a><img class="partner-logo" src="{{ asset('frontend/img/logos/custom/covenant.svg') }}" alt="" /></a>
                    </li>
                    <li>
                        <a><img class="partner-logo" src="{{ asset('frontend/img/logos/custom//infinite.svg') }}" alt="" /></a>
                    </li>
                    <li>
                        <a><img class="partner-logo" src="{{ asset('frontend/img/logos/custom/phasekit.svg') }}" alt="" /></a>
                    </li>
                    <li>
                        <a><img class="partner-logo" src="{{ asset('frontend/img/logos/custom/grubspot.svg') }}" alt="" /></a>
                    </li>
                    <li>
                        <a><img class="partner-logo" src="{{ asset('frontend/img/logos/custom/gutwork.svg') }}" alt="" /></a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>

<!-- Services section -->
<div id="services" class="section is-medium">
    <div class="container">
        <!-- Title -->
        <div class="section-title-wrapper has-text-centered">
            <div class="bg-number">1</div>
            <h2 class="section-title-landing">Who we are</h2>
            <h4>"We are the missing link between shoppers & retailers"</h4>
        </div>

        <div class="content-wrapper">
            <div class="columns is-vcentered">
                <!-- Content -->
                <div class="column is-11">
                    <div class="service-box">
                        <p style="text-align: center">
                            YOPA is a platform that connects shoppers
                            with retailers during sales discounts, and
                            promotion periods.
                        </p>
                        <p style="text-align: center">
                            We allow users to find deals, updates and
                            promotions with the help of their social
                            connections, family, co-workers and friends.
                        </p>
                        <p style="text-align: center">
                            On the flip side, we assist retailers and
                            e-commerce stores to drive traffic and sales.
                        </p>
                    </div>
                </div>
                <!-- Icons -->
            </div>
        </div>
    </div>
</div>

<!-- Feature -->
<div class="section section-feature-grey">
    <div class="container">
        <div class="columns is-vcentered">
            <!-- With the help of responsive modifiers, the text column is duplicated to ensure better readability both on desktop and mobile viewports -->
            <div class="column is-4 is-offset-1 is-hidden-desktop is-hidden-tablet">
                <div class="icon-subtitle"><i class="im im-icon-Calendar-4"></i></div>
                <div class="section-title-wrapper has-text-centered">
                    <h2 class="section-title-landing">How the App Works</h2>
                    <h4>We connect shoppers with retailers increasing both brand awareness and sales.</h4>
                    <div class="bg-number">1</div>
                </div>
                <div class="title-divider is-small"></div>
            </div>

            <div class="column is-6 has-text-centered">
                <img class="featured-svg" style="max-width: 550px" src="{{ asset('frontend/img/graphics/how-the-app-works.png') }}" data-base-url="{{ asset('frontend/img/graphics/how-the-app-works.png') }}" data-extension=".png" alt="" />
            </div>

            <div class="column is-4 is-offset-1 is-hidden-mobile">
                <div class="icon-subtitle"><i class="im im-icon-Calendar-4"></i></div>
                <div class="section-title-wrapper">
                    <h2 class="section-title-landing">How the App Works</h2>
                    <h4>We connect shoppers with retailers increasing both brand awareness and sales.</h4>
                    <div class="bg-number">1</div>
                </div>
                <div class="title-divider is-small"></div>
            </div>
        </div>
    </div>
</div>

<!-- Feature -->
<div class="section">
    <div class="container">
        <div class="columns is-vcentered">
            <div class="column is-4 is-offset-1">
                <div class="icon-subtitle">
                    <i class="im im-icon-Upload-toCloud"></i>
                </div>
                <div class="title quick-feature is-handwritten text-bold">
                    <h2 class="section-title-landing">Retailer Backend & Login</h2>
                    <div class="bg-number">2</div>
                </div>
                <div class="title-divider is-small"></div>
                <span class="body-text">All retailers get their own backend dashboard to manage their promotions and deals.</span>
            </div>

            <div class="column is-6 is-offset-1 has-text-centered">
                <img class="featured-svg" src="{{ asset('frontend/img/graphics/retailer-backend-and-login.png') }}" data-base-url="{{ asset('frontend/img/graphics/widgets/mini-widget-2') }}" data-extension=".png" alt="" />
            </div>
        </div>
    </div>
</div>

<!-- Feature -->
<div class="section section-feature-grey">
    <div class="container">
        <div class="columns is-vcentered">
            <!-- With the help of responsive modifiers, the text column is duplicated to ensure better readability both on desktop and mobile viewports -->
            <div class="column is-4 is-offset-1 is-hidden-desktop is-hidden-tablet">
                <div class="icon-subtitle">
                    <i class="im im-icon-Security-Check"></i>
                </div>
                <div class="title quick-feature is-handwritten text-bold">
                    <h2>User Friendly & Easy to Use</h2>
                    <div class="bg-number">3</div>
                </div>
                <div class="title-divider is-small"></div>
                <span class="body-text">When it comes to the consumer, the YOPA app is extremely simple to use.</span>
            </div>

            <div class="column is-6 has-text-centered">
                <img class="featured-svg" src="{{ asset('frontend/img/graphics/widgets/mini-widget-3-core.png') }}" data-base-url="{{ asset('frontend/img/graphics/widgets/mini-widget-3') }}" data-extension=".png" alt="" />
            </div>

            <div class="column is-4 is-offset-1 is-hidden-mobile">
                <div class="icon-subtitle">
                    <i class="im im-icon-Security-Check"></i>
                </div>
                <div class="title quick-feature is-handwritten text-bold">
                    <div>User Friendly & Easy to Use</div>
                    <div class="bg-number">3</div>
                </div>
                <div class="title-divider is-small"></div>
                <span class="body-text">When it comes to the consumer, the YOPA app is extremely simple to use.</span>
            </div>
        </div>
    </div>
</div>


<!-- Feature -->
<div class="section section-feature-grey">
    <div class="container">
        <div class="columns is-vcentered">
            <!-- With the help of responsive modifiers, the text column is duplicated to ensure better readability both on desktop and mobile viewports -->
            <div class="column is-4 is-offset-1 is-hidden-desktop is-hidden-tablet">
                <div class="icon-subtitle">
                    <i class="im im-icon-Security-Check"></i>
                </div>
                <div class="title quick-feature is-handwritten text-bold">
                    <h2>How We Drive Awareness & Sales</h2>
                    <div class="bg-number">3</div>
                </div>
                <div class="title-divider is-small"></div>
                <span class="body-text">We make use of location and interest based targeting.</span>
            </div>

            <div class="column is-6 has-text-centered">
                <img class="featured-svg" src="{{ asset('frontend/img/graphics/how-we-drive-awareness.png') }}" data-base-url="{{ asset('frontend/img/graphics/widgets/mini-widget-3') }}" data-extension=".png" alt="" />
            </div>

            <div class="column is-4 is-offset-1 is-hidden-mobile">
                <div class="icon-subtitle">
                    <i class="im im-icon-Security-Check"></i>
                </div>
                <div class="title quick-feature is-handwritten text-bold">
                    <div>How We Drive Awareness & Sales</div>
                    <div class="bg-number">3</div>
                </div>
                <div class="title-divider is-small"></div>
                <span class="body-text">We make use of location and interest based targeting.</span>
            </div>
        </div>
    </div>
</div>

<!--Mockup Section-->
<div class="section is-medium">
    <div class="container">
        <!-- Title -->
        <div class="section-title-wrapper pb-60">
            <div class="bg-number">5</div>
            <h2 class="title section-title has-text-centered dark-text text-bold">
                How the App Looks
            </h2>
            <h4 class="has-text-centered">
                The Search Page of the App
            </h4>
        </div>

        <!--Mockup-->
        <div class="mockup-wrap">
            <div class="image-wrap">
                <img class="full-image" src="{{ asset('frontend/img/graphics/how-the-app-works-1.png') }}" alt="" />
            </div>
        </div>

        <div class="section-title-wrapper pb-60 mt-20">
            <div class="bg-number">7</div>
            <h4 class="has-text-centered">
                The Deals Page of the App
            </h4>
        </div>

        <div class="mockup-wrap">
            <div class="image-wrap">
                <img class="full-image" src="{{ asset('frontend/img/graphics/how-the-app-works-2.png') }}" alt="" />
            </div>
        </div>
    </div>
</div>

{{--<!-- Clients grid -->--}}
{{--<div id="trust" class="section is-medium section-feature-grey">--}}
{{--    <div class="container">--}}
{{--        <!-- Title -->--}}
{{--        <div class="section-title-wrapper">--}}
{{--            <div class="bg-number"><i class="material-icons">domain</i></div>--}}
{{--            <h2 class="section-title-landing has-text-centered dark-text">--}}
{{--                We build Trust.--}}
{{--            </h2>--}}
{{--            <h4 class="has-text-centered">--}}
{{--                More than <b>900 Teams</b> use our product.--}}
{{--            </h4>--}}
{{--        </div>--}}
{{--        <!-- Grid -->--}}
{{--        <div class="content-wrapper">--}}
{{--            <div class="grid-clients five-grid">--}}
{{--                <div class="columns is-vcentered">--}}
{{--                    <div class="column is-hidden-mobile"></div>--}}
{{--                    <!-- Client -->--}}
{{--                    <div class="column">--}}
{{--                        <a><img class="client" src="{{ asset('frontend/img/logos/custom/gutwork.svg') }}" alt="" /></a>--}}
{{--                    </div>--}}
{{--                    <!-- Client -->--}}
{{--                    <div class="column">--}}
{{--                        <a><img class="client" src="{{ asset('frontend/img/logos/custom/phasekit.svg') }}" alt="" /></a>--}}
{{--                    </div>--}}
{{--                    <!-- Client -->--}}
{{--                    <div class="column">--}}
{{--                        <a><img class="client" src="{{ asset('frontend/img/logos/custom/grubspot.svg') }}" alt="" /></a>--}}
{{--                    </div>--}}
{{--                    <!-- Client -->--}}
{{--                    <div class="column">--}}
{{--                        <a><img class="client" src="{{ asset('frontend/img/logos/custom/taskbot.svg') }}" alt="" /></a>--}}
{{--                    </div>--}}
{{--                    <!-- Client -->--}}
{{--                    <div class="column">--}}
{{--                        <a><img class="client" src="{{ asset('frontend/img/logos/custom/systek.svg') }}" alt="" /></a>--}}
{{--                    </div>--}}
{{--                    <div class="column is-hidden-mobile"></div>--}}
{{--                </div>--}}
{{--                <div class="columns is-vcentered is-separator">--}}
{{--                    <div class="column is-hidden-mobile"></div>--}}
{{--                    <!-- Client -->--}}
{{--                    <div class="column">--}}
{{--                        <a><img class="client" src="{{ asset('frontend/img/logos/custom/infinite.svg') }}" alt="" /></a>--}}
{{--                    </div>--}}
{{--                    <!-- Client -->--}}
{{--                    <div class="column">--}}
{{--                        <a><img class="client" src="{{ asset('frontend/img/logos/custom/tribe.svg') }}" alt="" /></a>--}}
{{--                    </div>--}}
{{--                    <!-- Client -->--}}
{{--                    <div class="column">--}}
{{--                        <a><img class="client" src="{{ asset('frontend/img/logos/custom/powerball.svg') }}" alt="" /></a>--}}
{{--                    </div>--}}
{{--                    <!-- Client -->--}}
{{--                    <div class="column">--}}
{{--                        <a><img class="client" src="{{ asset('frontend/img/logos/custom/kromo.svg') }}" alt="" /></a>--}}
{{--                    </div>--}}
{{--                    <!-- Client -->--}}
{{--                    <div class="column">--}}
{{--                        <a><img class="client" src="{{ asset('frontend/img/logos/custom/covenant.svg') }}" alt="" /></a>--}}
{{--                    </div>--}}
{{--                    <div class="column is-hidden-mobile"></div>--}}
{{--                </div>--}}
{{--                <div class="columns is-vcentered is-separator">--}}
{{--                    <div class="column is-hidden-mobile"></div>--}}
{{--                    <!-- Client -->--}}
{{--                    <div class="column">--}}
{{--                        <a><img class="client" src="{{ asset('frontend/img/logos/custom/bitbreaker.svg') }}" alt="" /></a>--}}
{{--                    </div>--}}
{{--                    <!-- Client -->--}}
{{--                    <div class="column">--}}
{{--                        <a><img class="client" src="{{ asset('frontend/img/logos/custom/evently.svg') }}" alt="" /></a>--}}
{{--                    </div>--}}
{{--                    <!-- Client -->--}}
{{--                    <div class="column">--}}
{{--                        <a><img class="client" src="{{ asset('frontend/img/logos/custom/proactive.svg') }}" alt="" /></a>--}}
{{--                    </div>--}}
{{--                    <!-- Client -->--}}
{{--                    <div class="column">--}}
{{--                        <a><img class="client" src="{{ asset('frontend/img/logos/custom/transfuseio.svg') }}" alt="" /></a>--}}
{{--                    </div>--}}
{{--                    <!-- Client -->--}}
{{--                    <div class="column">--}}
{{--                        <a><img class="client" src="{{ asset('frontend/img/logos/custom/livetalk.svg') }}" alt="" /></a>--}}
{{--                    </div>--}}
{{--                    <div class="column is-hidden-mobile"></div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <!-- CTA -->--}}
{{--            <div class="has-text-centered is-title-reveal pt-40 pb-40">--}}
{{--                <a href="kit1-pricing.html" class="button button-cta btn-align primary-btn raised rounded">Get started Now</a>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
{{--<!-- Clients -->--}}

{{--<!-- Testimonials section -->--}}
{{--<div class="section is-medium">--}}
{{--    <div class="container">--}}
{{--        <!-- Title -->--}}
{{--        <div class="section-title-wrapper has-text-centered">--}}
{{--            <div class="bg-number">5</div>--}}
{{--            <h2 class="section-title-landing">Our customers love us</h2>--}}
{{--            <h4>Check out what they say about us</h4>--}}
{{--        </div>--}}

{{--        <div class="content-wrapper">--}}
{{--            <div class="columns">--}}
{{--                <div class="column"></div>--}}

{{--                <div class="column is-6">--}}
{{--                    <!-- Carousel wrapper -->--}}
{{--                    <div class="testimonials is-wavy">--}}
{{--                        <!-- Testimonial item -->--}}
{{--                        <div class="testimonial-item">--}}
{{--                            <div class="flex-card card-overflow raised">--}}
{{--                                <div class="testimonial-avatar">--}}
{{--                                    <img src="https://via.placeholder.com/250x250" alt="" data-demo-src="{{ asset('frontend/img/avatars/carolin.png') }}" />--}}
{{--                                </div>--}}
{{--                                <div class="testimonial-name">--}}
{{--                                    <h3>Marjory Cambell</h3>--}}
{{--                                    <span>CEO</span>--}}
{{--                                </div>--}}
{{--                                <div class="testimonial-content">--}}
{{--                                    <p>--}}
{{--                                        Lorem ipsum dolor sit amet, clita laoreet ne cum. His cu--}}
{{--                                        harum inermis iudicabit. Ex vidit fierent hendrerit eum, sed--}}
{{--                                        stet periculis ut. Vis in probo decore labitur. Unum simul--}}
{{--                                        an vis, tale patrioque eos ad, dicunt percipit ea nam. Vis--}}
{{--                                        dolor quidam assentior te, atomorum posidonium qui an.--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <!-- Testimonial item -->--}}
{{--                        <div class="testimonial-item">--}}
{{--                            <div class="flex-card card-overflow raised">--}}
{{--                                <div class="testimonial-avatar">--}}
{{--                                    <img src="https://via.placeholder.com/250x250" alt="" data-demo-src="{{ asset('frontend/img/avatars/terry.jpg') }}" />--}}
{{--                                </div>--}}
{{--                                <div class="testimonial-name">--}}
{{--                                    <h3>Terry Daniels</h3>--}}
{{--                                    <span>Software Engineer</span>--}}
{{--                                </div>--}}
{{--                                <div class="testimonial-content">--}}
{{--                                    <p>--}}
{{--                                        Lorem ipsum dolor sit amet, clita laoreet ne cum. His cu--}}
{{--                                        harum inermis iudicabit. Ex vidit fierent hendrerit eum, sed--}}
{{--                                        stet periculis ut. Vis in probo decore labitur. Unum simul--}}
{{--                                        an vis, tale patrioque eos ad, dicunt percipit ea nam. Vis--}}
{{--                                        dolor quidam assentior te, atomorum posidonium qui an.--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <!-- Testimonial item -->--}}
{{--                        <div class="testimonial-item">--}}
{{--                            <div class="flex-card card-overflow raised">--}}
{{--                                <div class="testimonial-avatar">--}}
{{--                                    <img src="https://via.placeholder.com/250x250" alt="" data-demo-src="{{ asset('frontend/img/avatars/stella.jpg') }}" />--}}
{{--                                </div>--}}
{{--                                <div class="testimonial-name">--}}
{{--                                    <h3>Stella Daniels</h3>--}}
{{--                                    <span>Head of Marketing</span>--}}
{{--                                </div>--}}
{{--                                <div class="testimonial-content">--}}
{{--                                    <p>--}}
{{--                                        Lorem ipsum dolor sit amet, clita laoreet ne cum. His cu--}}
{{--                                        harum inermis iudicabit. Ex vidit fierent hendrerit eum, sed--}}
{{--                                        stet periculis ut. Vis in probo decore labitur. Unum simul--}}
{{--                                        an vis, tale patrioque eos ad, dicunt percipit ea nam. Vis--}}
{{--                                        dolor quidam assentior te, atomorum posidonium qui an.--}}
{{--                                    </p>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}

{{--                <div class="column"></div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--</div>--}}
@endsection
