@extends('layouts/contentLayoutMaster')

@section('title', 'User Edit')

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
@endsection

@section('content')
    <!-- users list start -->
    <section>
        <livewire:user.show :user="$user" :add="$add" />
    </section>
@endsection
