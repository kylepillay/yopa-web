@extends('layouts/contentLayoutMaster')

@section('title', 'Deals List')

@section('page-style')
  {{-- Page Css files --}}
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
@endsection

@section('content')
<!-- users list start -->
<section>
    <livewire:deal.all />
</section>
@endsection
