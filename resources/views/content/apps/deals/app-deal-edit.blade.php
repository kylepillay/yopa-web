@extends('layouts/contentLayoutMaster')

@section('title', 'Deal Edit')

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
@endsection

@section('content')
    <!-- deals list start -->
    <section>
        <livewire:deal.show :deal="$deal" :add="$add" />
    </section>
@endsection
