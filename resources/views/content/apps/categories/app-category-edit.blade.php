@extends('layouts/contentLayoutMaster')

@section('title', 'Category Edit')

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
@endsection

@section('content')
    <!-- deals list start -->
    <section>
        <livewire:category.show :category="$category" :add="$add" />
    </section>
@endsection
