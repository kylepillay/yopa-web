@extends('layouts/contentLayoutMaster')

@section('title', 'Categories List')

@section('page-style')
  <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
@endsection

@section('content')
<section>
    <livewire:category.all />
</section>
@endsection
