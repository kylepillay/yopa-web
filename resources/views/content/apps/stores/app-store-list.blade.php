@extends('layouts/contentLayoutMaster')

@section('title', 'Stores List')

@section('content')
<section>
    <livewire:store.all />
</section>
@endsection
