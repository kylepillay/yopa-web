@extends('layouts/contentLayoutMaster')

@section('title', 'Store Edit')

@section('page-style')
    {{-- Page Css files --}}
    <link rel="stylesheet" href="{{ asset(mix('css/base/plugins/forms/form-validation.css')) }}">
@endsection

@section('content')
    <!-- deals list start -->
    <section>
        <livewire:store.show :store="$store" :add="$add" />
    </section>
@endsection
