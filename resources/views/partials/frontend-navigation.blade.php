<nav class="navbar navbar-wrapper navbar-default navbar-fade is-transparent">
    <div class="container">
        <!-- Brand -->
        <div class="navbar-brand">
            <a class="navbar-item" href="/">
                <img class="switcher-logo" src="{{ asset('frontend/img/logos/main-logo.png') }}" alt="">
            </a>

            <!-- Responsive toggle -->
            <div class="custom-burger" data-target="">
                <a id="" class="responsive-btn" href="javascript:void(0);">
                            <span class="menu-toggle">
                              <span class="icon-box-toggle">
                                  <span class="rotate">
                                      <i class="icon-line-top"></i>
                                      <i class="icon-line-center"></i>
                                      <i class="icon-line-bottom"></i>
                                  </span>
                            </span>
                            </span>
                </a>
            </div>
        </div>

        <!-- Navbar menu -->
        <div class="navbar-menu">
            <!-- Navbar Start -->
{{--            <div class="navbar-start">--}}
{{--                <!-- Navbar item -->--}}
{{--                <a class="navbar-item is-slide" href="kit1-features.html">--}}
{{--                    Features--}}
{{--                </a>--}}
{{--                <!-- Navbar item -->--}}
{{--                <a class="navbar-item is-slide" href="kit1-pricing.html">--}}
{{--                    Pricing--}}
{{--                </a>--}}
{{--                <!-- Navbar item -->--}}
{{--                <a class="navbar-item is-slide" href="kit1-login.html">--}}
{{--                    Login--}}
{{--                </a>--}}
{{--            </div>--}}

            <!-- Navbar end -->
            <div class="navbar-end">
                <!-- Signup button -->
                @if (Route::has('register') && !Auth::user())
                    <div class="navbar-item">
                        <a id="#login-btn" style="color: darkslategray;" href="{{ route('login') }}" class="button button-cta btn-outlined is-bold btn-align rounded raised">
                            Login
                        </a>
                    </div>
                    <div class="navbar-item">
                        <a id="#signup-btn" href="{{ route('register') }}" class="button button-cta btn-outlined is-bold btn-align primary-btn rounded raised">
                            Sign Up
                        </a>
                    </div>
                @else
                    <div class="navbar-item">
                        <a id="#signup-btn" href="{{ route('dashboard') }}" class="button button-cta btn-outlined is-bold btn-align primary-btn rounded raised">
                            Go to Dashboard
                        </a>
                    </div>
                @endif

            </div>
        </div>
    </div>
</nav>

