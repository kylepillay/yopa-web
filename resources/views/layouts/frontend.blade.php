<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required Meta Tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">

    <title> YoPa :: Home</title>
    <link rel="icon" type="image/png" href="{{ asset('frontend/img/favicon.png') }}" />

    <!--Core CSS -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
    <link href="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.css" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('frontend/css/app.css') }}">
    <link id="theme-sheet" rel="stylesheet" href="{{ asset('frontend/css/teal.css') }}">
</head>

<body class="is-theme-core">
<div class="pageloader"></div>
<div class="infraloader is-active"></div>


    @yield('content')

    <!-- Dark footer -->
    <footer id="dark-footer" class="footer footer-dark">
        <div class="container">
            <div class="columns">
                <div class="column">
                    <div class="footer-column">
                        <div class="footer-logo">
                            <img class="switcher-logo-w" src="{{ asset('frontend/img/logos/main-logo.png') }}" alt="">
                        </div>
                        <div class="footer-header">
                            <nav class="level is-mobile">
                                <div class="level-left level-social">
                                    <a href="#" class="level-item">
                                        <span class="icon"><i class="fa fa-facebook"></i></span>
                                    </a>
                                    <a href="#" class="level-item">
                                        <span class="icon"><i class="fa fa-twitter"></i></span>
                                    </a>
                                    <a href="#" class="level-item">
                                        <span class="icon"><i class="fa fa-linkedin"></i></span>
                                    </a>
                                    <a href="#" class="level-item">
                                        <span class="icon"><i class="fa fa-dribbble"></i></span>
                                    </a>
                                    <a href="#" class="level-item">
                                        <span class="icon"><i class="fa fa-github"></i></span>
                                    </a>
                                </div>
                            </nav>
                        </div>
                        <div class="copyright">
                            <span class="moto light-text">Contact us at a.s@itsyopa.com.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- /Dark footer -->
    <!-- Back To Top Button -->
    <div id="backtotop"><a href="#"></a></div>
    <script src="https://api.mapbox.com/mapbox-gl-js/v1.12.0/mapbox-gl.js"></script>
    <script src="{{ asset('frontend/js/app.js') }}"></script>
    <script src="{{ asset('frontend/js/core.js') }}"></script>
</body>

</html>
