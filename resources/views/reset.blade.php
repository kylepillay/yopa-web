@extends('layouts.main')

@section('content')
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form style="max-width: 400px; margin: auto; margin-top: 200px" action="{{url('/api/auth/password/reset')}}" method="post">
        <h1 class="h3 mb-3 font-weight-normal">Please choose a new password</h1>
        <label for="inputEmail" class="sr-only">Password</label>
        <input style="margin-bottom: 20px" type="password" name="password" id="password" class="form-control" placeholder="Password" required
               autofocus>
        <label for="inputPassword" class="sr-only">Confirm Password</label>
        <input style="margin-bottom: 20px" type="password" id="password_confirmation" class="form-control" placeholder="Password Confirmation"
               name="password_confirmation" required>
        <input type="hidden" name="email" value="{{$email}}">
        <input type="hidden" name="token" value="{{$token}}">
        <button class="btn btn-lg btn-primary btn-block" type="submit">Reset Password</button>
        <p class="mt-5 mb-3 text-muted">&copy; YoPa</p>
    </form>
@endsection
