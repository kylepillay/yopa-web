<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('role')->nullable();
            $table->string('phone')->nullable();
            $table->string('city')->nullable();
            $table->string('featured_image')->nullable();
            $table->bigInteger('featured_image_id')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('avatar')->default('avatar.png');
            $table->text('address')->nullable();
            $table->text('description')->nullable();
            $table->text('settings')->nullable();
            $table->text('shortcuts')->nullable();
            $table->string('password');
            $table->boolean('active')->default(false);
            $table->string('activation_token')->nullable();
            $table->boolean('allow_push_notifications')->default(true);
            $table->boolean('allow_email_notifications')->default(true);
            $table->text('expo_push_token')->nullable();
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
