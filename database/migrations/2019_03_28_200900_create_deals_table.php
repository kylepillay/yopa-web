<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDealsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('handle');
            $table->longText('description');
            $table->longText('image')->nullable();
            $table->bigInteger('category_id')->default(1);
            $table->decimal('price');
            $table->decimal('salePrice');
            $table->bigInteger('user_id');
            $table->bigInteger('store_id');
            $table->text('link')->nullable();
            $table->string('latitude');
            $table->string('longitude');
            $table->string('province_id');
            $table->integer('active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deals');
    }
}
