<?php

namespace Database\Seeders;

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\Models\Deal;

class DealsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws \Spatie\MediaLibrary\MediaCollections\Exceptions\FileCannotBeAdded
     */
    public function run()
    {
        Deal::truncate();

        $faker = \Faker\Factory::create();

        $deal = Deal::create([
            'name' => '2 Minute Noodles',
            'handle' => '2-minute-noodles',
            'description' => $faker->paragraph,
            'image' => '1.jpg',
            'link' => 'https://www.makro.co.za/makstorefront/makro/en/electronics-computers/computers-tablets/laptops-notebooks/notebooks/hp-39-cm-15-series-intel-celeron-laptop-/p/000000000000341106_EA',
            'category_id' => 1,
            'price' => 205.00,
            'salePrice' => 180.00,
            'user_id' => 2,
            'store_id' => 1,
            'province_id' => $faker->randomDigitNot(0),
            'latitude' => -26.041172,
            'longitude' => 28.054978,
            'active' => 1,
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $deal->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $deal->attachTags(['tag 1', 'tag 2', 'tag 3']);

        $deal = Deal::create([
            'name' => 'LG Microwave',
            'handle' => 'lg-microwave',
            'description' => $faker->paragraph,
            'image' => '2.jpg',
            'link' => 'https://www.makro.co.za/makstorefront/makro/en/electronics-computers/computers-tablets/laptops-notebooks/notebooks/hp-39-cm-15-series-intel-celeron-laptop-/p/000000000000341106_EA',
            'category_id' => 4,
            'price' => 305.00,
            'salePrice' => 228.00,
            'user_id' => 2,
            'province_id' => $faker->randomDigitNot(0),
            'store_id' => 1,
            'latitude' => -26.030325,
            'longitude' => 28.070968,
            'active' => 1,
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $deal->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $deal->attachTags(['tag 1', 'tag 2', 'tag 3']);

        $deal = Deal::create([
            'name' => 'Air Bed',
            'handle' => 'air-bed',
            'description' => $faker->paragraph,
            'image' => '3.jpg',
            'link' => 'https://www.makro.co.za/makstorefront/makro/en/electronics-computers/computers-tablets/laptops-notebooks/notebooks/hp-39-cm-15-series-intel-celeron-laptop-/p/000000000000341106_EA',
            'category_id' => 5,
            'price' => 405.00,
            'salePrice' => 328.00,
            'user_id' => 2,
            'province_id' => $faker->randomDigitNot(0),
            'store_id' => 1,
            'latitude' => -26.031062,
            'longitude' => 28.082921,
            'active' => 1,
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $deal->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $deal->attachTags(['tag 1', 'tag 2', 'tag 3']);

        $deal = Deal::create([
            'name' => 'RC Plane',
            'handle' => 'rc-plane',
            'description' => $faker->paragraph,
            'image' => '4.jpg',
            'link' => 'https://www.makro.co.za/makstorefront/makro/en/electronics-computers/computers-tablets/laptops-notebooks/notebooks/hp-39-cm-15-series-intel-celeron-laptop-/p/000000000000341106_EA',
            'category_id' => 3,
            'price' => 305.00,
            'salePrice' => 228.00,
            'user_id' => 3,
            'province_id' => $faker->randomDigitNot(0),
            'store_id' => 4,
            'latitude' => -26.053484,
            'longitude' => 28.062616,
            'active' => 1,
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $deal->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $deal->attachTags(['tag 1', 'tag 2', 'tag 3']);

        $deal = Deal::create([
            'name' => 'Queen Sized Bed',
            'handle' => 'queen-sized-bed',
            'description' => $faker->paragraph,
            'image' => '5.jpg',
            'link' => 'https://www.makro.co.za/makstorefront/makro/en/electronics-computers/computers-tablets/laptops-notebooks/notebooks/hp-39-cm-15-series-intel-celeron-laptop-/p/000000000000341106_EA',
            'category_id' => 8,
            'price' => 505.00,
            'salePrice' => 424.00,
            'user_id' => 3,
            'province_id' => $faker->randomDigitNot(0),
            'store_id' => 2,
            'latitude' => -26.039771,
            'longitude' => 28.019802,
            'active' => 1,
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $deal->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $deal->attachTags(['tag 1', 'tag 2', 'tag 3']);

        $deal = Deal::create([
            'name' => '24 Inch Monitor',
            'handle' => '24-inch-monitor',
            'description' => $faker->paragraph,
            'image' => '6.jpg',
            'link' => 'https://www.makro.co.za/makstorefront/makro/en/electronics-computers/computers-tablets/laptops-notebooks/notebooks/hp-39-cm-15-series-intel-celeron-laptop-/p/000000000000341106_EA',
            'category_id' => 2,
            'price' => 405.00,
            'salePrice' => 328.00,
            'province_id' => $faker->randomDigitNot(0),
            'user_id' => 3,
            'store_id' => 4,
            'latitude' => -29.566197,
            'longitude' => 30.384864,
            'active' => 1,
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $deal->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $deal->attachTags(['tag 1', 'tag 2', 'tag 3']);

        $deal = Deal::create([
            'name' => 'Keyboard',
            'handle' => 'keyboard',
            'description' => $faker->paragraph,
            'image' => '7.jpg',
            'link' => 'https://www.makro.co.za/makstorefront/makro/en/electronics-computers/computers-tablets/laptops-notebooks/notebooks/hp-39-cm-15-series-intel-celeron-laptop-/p/000000000000341106_EA',
            'category_id' => 9,
            'price' => 405.00,
            'salePrice' => 328.00,
            'user_id' => 3,
            'province_id' => $faker->randomDigitNot(0),
            'store_id' => 5,
            'latitude' => -29.561058,
            'longitude' => 30.418233,
            'active' => 0,
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $deal->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $deal->attachTags(['tag 1', 'tag 2', 'tag 3']);

        $deal = Deal::create([
            'name' => 'Toothpaste',
            'handle' => 'toothpaste',
            'description' => $faker->paragraph,
            'image' => '8.jpg',
            'link' => 'https://www.makro.co.za/makstorefront/makro/en/electronics-computers/computers-tablets/laptops-notebooks/notebooks/hp-39-cm-15-series-intel-celeron-laptop-/p/000000000000341106_EA',
            'category_id' => 7,
            'price' => 45.00,
            'salePrice' => 38.50,
            'user_id' => 3,
            'province_id' => $faker->randomDigitNot(0),
            'store_id' => 4,
            'latitude' => -29.566060,
            'longitude' => 30.414016,
            'active' => 0,
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $deal->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $deal->attachTags(['tag 1', 'tag 2', 'tag 3']);

        $deal = Deal::create([
            'name' => 'Chocolate',
            'handle' => 'toothpaste',
            'description' => $faker->paragraph,
            'image' => '9.jpg',
            'link' => 'https://www.makro.co.za/makstorefront/makro/en/electronics-computers/computers-tablets/laptops-notebooks/notebooks/hp-39-cm-15-series-intel-celeron-laptop-/p/000000000000341106_EA',
            'category_id' => 3,
            'price' => 25.00,
            'salePrice' => 18.00,
            'user_id' => 2,
            'province_id' => $faker->randomDigitNot(0),
            'store_id' => 2,
            'latitude' => -29.865088,
            'longitude' => 30.973945,
            'active' => 1,
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $deal->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $deal->attachTags(['tag 1', 'tag 2', 'tag 3']);

        $deal = Deal::create([
            'name' => 'Bananas',
            'handle' => 'bananas',
            'description' => $faker->paragraph,
            'image' => '10.jpg',
            'link' => 'https://www.makro.co.za/makstorefront/makro/en/electronics-computers/computers-tablets/laptops-notebooks/notebooks/hp-39-cm-15-series-intel-celeron-laptop-/p/000000000000341106_EA',
            'category_id' => 5,
            'price' => 45.00,
            'salePrice' => 38.00,
            'user_id' => 2,
            'province_id' => $faker->randomDigitNot(0),
            'store_id' => 1,
            'latitude' => -28.198314,
            'longitude' => 29.077043,
            'active' => 1,
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $deal->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $deal->attachTags(['tag 1', 'tag 2', 'tag 3']);

        $deal = Deal::create([
            'name' => 'Peanuts',
            'handle' => 'peanuts',
            'description' => $faker->paragraph,
            'image' => '11.jpg',
            'link' => 'https://www.makro.co.za/makstorefront/makro/en/electronics-computers/computers-tablets/laptops-notebooks/notebooks/hp-39-cm-15-series-intel-celeron-laptop-/p/000000000000341106_EA',
            'category_id' => 8,
            'price' => 23.00,
            'salePrice' => 14.00,
            'user_id' => 2,
            'province_id' => $faker->randomDigitNot(0),
            'store_id' => 2,
            'latitude' => -28.179261,
            'longitude' => 28.280500,
            'active' => 1,
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $deal->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $deal->attachTags(['tag 1', 'tag 2', 'tag 3']);

        $deal = Deal::create([
            'name' => 'Computer',
            'handle' => 'computer',
            'description' => $faker->paragraph,
            'image' => '12.jpg',
            'link' => 'https://www.makro.co.za/makstorefront/makro/en/electronics-computers/computers-tablets/laptops-notebooks/notebooks/hp-39-cm-15-series-intel-celeron-laptop-/p/000000000000341106_EA',
            'category_id' => 5,
            'price' => 4305.00,
            'salePrice' => 3238.00,
            'user_id' => 2,
            'province_id' => $faker->randomDigitNot(0),
            'store_id' => 2,
            'latitude' => -26.120802,
            'longitude' => 27.981817,
            'active' => 1,
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $deal->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $deal->attachTags(['tag 1', 'tag 2', 'tag 3']);

        $deal = Deal::create([
            'name' => 'Bean Bag',
            'handle' => 'bean-bag',
            'description' => $faker->paragraph,
            'image' => '13.jpg',
            'link' => 'https://www.makro.co.za/makstorefront/makro/en/electronics-computers/computers-tablets/laptops-notebooks/notebooks/hp-39-cm-15-series-intel-celeron-laptop-/p/000000000000341106_EA',
            'category_id' => 4,
            'price' => 405.00,
            'salePrice' => 328.00,
            'user_id' => 2,
            'province_id' => $faker->randomDigitNot(0),
            'store_id' => 3,
            'latitude' => -26.115714,
            'longitude' => 27.925791,
            'active' => 1,
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $deal->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $deal->attachTags(['tag 1', 'tag 2', 'tag 3']);

        $deal = Deal::create([
            'name' => 'Cranberry Jam',
            'handle' => 'cranberry-jam',
            'description' => $faker->paragraph,
            'image' => '14.jpg',
            'link' => 'https://www.makro.co.za/makstorefront/makro/en/electronics-computers/computers-tablets/laptops-notebooks/notebooks/hp-39-cm-15-series-intel-celeron-laptop-/p/000000000000341106_EA',
            'category_id' => 5,
            'price' => 35.00,
            'salePrice' => 28.00,
            'user_id' => 3,
            'store_id' => 5,
            'province_id' => $faker->randomDigitNot(0),
            'latitude' => -26.105339,
            'longitude' => 27.953935,
            'active' => 1,
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $deal->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $deal->attachTags(['tag 1', 'tag 2', 'tag 3']);

        $deal = Deal::create([
            'name' => 'Simba Chips',
            'handle' => 'simba-chips',
            'description' => $faker->paragraph,
            'image' => '15.jpg',
            'link' => 'https://www.makro.co.za/makstorefront/makro/en/electronics-computers/computers-tablets/laptops-notebooks/notebooks/hp-39-cm-15-series-intel-celeron-laptop-/p/000000000000341106_EA',
            'category_id' => 5,
            'price' => 12.00,
            'salePrice' => 8.00,
            'user_id' => 3,
            'province_id' => $faker->randomDigitNot(0),
            'store_id' => 5,
            'latitude' => -26.105339,
            'longitude' => 27.953935,
            'active' => 1,
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $deal->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $deal->attachTags(['tag 1', 'tag 2', 'tag 3']);
    }
}
