<?php
namespace Database\Seeders;

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\Models\Category;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileCannotBeAdded;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws FileCannotBeAdded
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function run()
    {
        Category::truncate();

        $category = Category::create([
            'name' => 'Uncategorized'
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $category->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $category = Category::create([
            'name' => 'Sports Equipment'
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $category->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $category = Category::create([
            'name' => 'Clothing'
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $category->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $category = Category::create([
            'name' => 'Groceries'
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $category->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $category = Category::create([
            'name' => 'Food'
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $category->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $category = Category::create([
            'name' => 'Electronics'
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $category->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $category = Category::create([
            'name' => 'Cosmetics'
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $category->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $category = Category::create([
            'name' => 'Toiletries'
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $category->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $category = Category::create([
            'name' => 'Furniture'
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $category->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $category = Category::create([
            'name' => 'Supplements'
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $category->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');
    }
}
