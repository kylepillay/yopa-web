<?php

namespace Database\Seeders;

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\Models\Store;

class StoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Store::truncate();

        $faker = \Faker\Factory::create();

        $store = Store::create([
            'name' => 'Makro',
            'description' => $faker->paragraph,
            'address' => $faker->paragraph,
            'latitude' => $faker->latitude,
            'longitude' => $faker->longitude,
            'province_id' => $faker->randomDigitNot(0),
            'user_id' => 2,
            'active' => 1,
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $store->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $store->attachTags(['tag 1', 'tag 2', 'tag 3']);

        $store = Store::create([
            'name' => 'Game',
            'description' => $faker->paragraph,
            'address' => $faker->paragraph,
            'latitude' => $faker->latitude,
            'longitude' => $faker->longitude,
            'province_id' => $faker->randomDigitNot(0),
            'user_id' => 2,
            'active' => 1,
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $store->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $store->attachTags(['tag 1', 'tag 2', 'tag 3']);

        $store = Store::create([
            'name' => 'Checkers',
            'description' => $faker->paragraph,
            'address' => $faker->paragraph,
            'latitude' => $faker->latitude,
            'longitude' => $faker->longitude,
            'province_id' => $faker->randomDigitNot(0),
            'user_id' => 2,
            'active' => 1,
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $store->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $store->attachTags(['tag 1', 'tag 2', 'tag 3']);

        $store = Store::create([
            'name' => 'Edgars',
            'description' => $faker->paragraph,
            'address' => $faker->paragraph,
            'latitude' => $faker->latitude,
            'longitude' => $faker->longitude,
            'province_id' => $faker->randomDigitNot(0),
            'user_id' => 3,
            'active' => 1,
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $store->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $store->attachTags(['tag 1', 'tag 2', 'tag 3']);

        $store = Store::create([
            'name' => 'Jet',
            'description' => $faker->paragraph,
            'address' => $faker->paragraph,
            'latitude' => $faker->latitude,
            'longitude' => $faker->longitude,
            'province_id' => $faker->randomDigitNot(0),
            'user_id' => 3,
            'active' => 1,
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $store->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $store->attachTags(['tag 1', 'tag 2', 'tag 3']);

        $store = Store::create([
            'name' => 'Spar',
            'description' => $faker->paragraph,
            'address' => $faker->paragraph,
            'latitude' => $faker->latitude,
            'longitude' => $faker->longitude,
            'province_id' => $faker->randomDigitNot(0),
            'user_id' => 3,
            'active' => 1,
        ]);

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $store->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $store->attachTags(['tag 1', 'tag 2', 'tag 3']);

    }
}
