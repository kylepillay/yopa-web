<?php

namespace Database\Seeders;

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileCannotBeAdded;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileDoesNotExist;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileIsTooBig;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     * @throws FileCannotBeAdded
     * @throws FileDoesNotExist
     * @throws FileIsTooBig
     */
    public function run()
    {
        User::truncate();

        $faker = Faker::create();

        // Hash a password before loop to save resource usage
        $password = bcrypt('secret');

        $user = User::create([
            'name' => 'Yopa Administrator',
            'email' => 'admin@itsyopa.com',
            'active' => true,
            'role' => 'admin',
            'email_verified_at' => $faker->dateTime('now'),
            'phone' => $faker->phoneNumber,
            'city' => $faker->city,
            'address' => $faker->address,
            'description' => $faker->paragraph,
            'password' => $password,
        ]);

        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $user->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $user = User::create([
            'name' => 'Yopa Company',
            'email' => 'company@itsyopa.com',
            'active' => true,
            'role' => 'staff',
            'email_verified_at' => $faker->dateTime('now'),
            'phone' => $faker->phoneNumber,
            'city' => $faker->city,
            'address' => $faker->address,
            'description' => $faker->paragraph,
            'password' => $password,
        ]);

        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $user->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        for ($i = 0; $i < 20; $i++) {
            $user = User::create([
                'name' => $faker->name,
                'email' => $faker->email,
                'email_verified_at' => $faker->dateTime('now'),
                'phone' => $faker->phoneNumber,
                'city' => $faker->city,
                'active' => true,
                'role' => 'user',
                'password' => $password,
            ]);
            $imageUrl = $faker->imageUrl(640, 480, null, false);
            $user->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');
        }
    }
}
