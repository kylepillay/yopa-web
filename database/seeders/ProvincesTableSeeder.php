<?php

namespace Database\Seeders;

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use App\Models\Province;

class ProvincesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Province::truncate();


        $province = Province::create([
            'name' => 'Eastern Cape'
        ]);

        $province = Province::create([
            'name' => 'Free State'
        ]);

        $province = Province::create([
            'name' => 'Gauteng'
        ]);

        $province = Province::create([
            'name' => 'KwaZulu-Natal'
        ]);

        $province = Province::create([
            'name' => 'Limpopo'
        ]);

        $province = Province::create([
            'name' => 'Mpumalanga'
        ]);

        $province = Province::create([
            'name' => 'Northern Cape'
        ]);

        $province = Province::create([
            'name' => 'North West'
        ]);

    }
}
