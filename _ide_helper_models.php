<?php

// @formatter:off
/**
 * A helper file for your Eloquent Models
 * Copy the phpDocs from this file to the correct Model,
 * And remove them from this file, to prevent double declarations.
 *
 * @author Barry vd. Heuvel <barryvdh@gmail.com>
 */


namespace App\Models{
/**
 * App\Models\Category
 *
 * @property-read Collection|Deal[] $deals
 * @method static Builder|Category newModelQuery()
 * @method static Builder|Category newQuery()
 * @method static Builder|Category query()
 * @mixin Eloquent
 * @property int $id
 * @property string $name
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Category whereCreatedAt($value)
 * @method static Builder|Category whereId($value)
 * @method static Builder|Category whereName($value)
 * @method static Builder|Category whereUpdatedAt($value)
 * @property-read int|null $deals_count
 */
	class Category extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Deal
 *
 * @property-read Category $category
 * @property-read mixed $favourited
 * @property-read mixed $grabbed
 * @property-read mixed $smashed
 * @property Collection|Tag[] $tags
 * @property-read Store $store
 * @property-read User $user
 * @method static Builder|Deal newModelQuery()
 * @method static Builder|Deal newQuery()
 * @method static Builder|Deal query()
 * @method static Builder|Deal withAllTags($tags, $type = null)
 * @method static Builder|Deal withAllTagsOfAnyType($tags)
 * @method static Builder|Deal withAnyTags($tags, $type = null)
 * @method static Builder|Deal withAnyTagsOfAnyType($tags)
 * @mixin Eloquent
 * @property int $id
 * @property string $name
 * @property string $handle
 * @property string $description
 * @property string|null $image
 * @property int $category_id
 * @property float $price
 * @property float $salePrice
 * @property int $user_id
 * @property int $store_id
 * @property string|null $link
 * @property string $latitude
 * @property string $longitude
 * @property int $active
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Deal whereActive($value)
 * @method static Builder|Deal whereCategoryId($value)
 * @method static Builder|Deal whereCreatedAt($value)
 * @method static Builder|Deal whereDescription($value)
 * @method static Builder|Deal whereHandle($value)
 * @method static Builder|Deal whereId($value)
 * @method static Builder|Deal whereImage($value)
 * @method static Builder|Deal whereLatitude($value)
 * @method static Builder|Deal whereLink($value)
 * @method static Builder|Deal whereLongitude($value)
 * @method static Builder|Deal whereName($value)
 * @method static Builder|Deal wherePrice($value)
 * @method static Builder|Deal whereSalePrice($value)
 * @method static Builder|Deal whereStoreId($value)
 * @method static Builder|Deal whereUpdatedAt($value)
 * @method static Builder|Deal whereUserId($value)
 * @property string|null $featured_image
 * @property int|null $featured_image_id
 * @property string $avatar
 * @property-read mixed $avatar_url
 * @property-read mixed $categories
 * @property-read mixed $stores
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @property-read int|null $tags_count
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereFeaturedImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Deal whereFeaturedImageId($value)
 */
	class Deal extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Favourite
 *
 * @method static Builder|Favourite newModelQuery()
 * @method static Builder|Favourite newQuery()
 * @method static Builder|Favourite query()
 * @mixin Eloquent
 * @property int $user_id
 * @property int $deal_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Favourite whereCreatedAt($value)
 * @method static Builder|Favourite whereDealId($value)
 * @method static Builder|Favourite whereUpdatedAt($value)
 * @method static Builder|Favourite whereUserId($value)
 */
	class Favourite extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Image
 *
 * @property int $id
 * @property int $imageable_id
 * @property string $imageable_type
 * @property string $uri
 * @property int $public
 * @property int $height
 * @property int $width
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Model|\Eloquent $imageable
 * @method static \Illuminate\Database\Eloquent\Builder|Image newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Image newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Image query()
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereHeight($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereImageableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereImageableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image wherePublic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereUri($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Image whereWidth($value)
 */
	class Image extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Membership
 *
 * @property int $id
 * @property int $team_id
 * @property int $user_id
 * @property string|null $role
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Membership newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Membership newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Membership query()
 * @method static \Illuminate\Database\Eloquent\Builder|Membership whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Membership whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Membership whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Membership whereTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Membership whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Membership whereUserId($value)
 */
	class Membership extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Notification
 *
 * @property-read Deal $deal
 * @property-read User $user
 * @method static Builder|Notification newModelQuery()
 * @method static Builder|Notification newQuery()
 * @method static Builder|Notification query()
 * @mixin Eloquent
 * @property int $id
 * @property int $user_id
 * @property int $deal_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Notification whereCreatedAt($value)
 * @method static Builder|Notification whereDealId($value)
 * @method static Builder|Notification whereId($value)
 * @method static Builder|Notification whereUpdatedAt($value)
 * @method static Builder|Notification whereUserId($value)
 */
	class Notification extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\PasswordReset
 *
 * @method static Builder|PasswordReset newModelQuery()
 * @method static Builder|PasswordReset newQuery()
 * @method static Builder|PasswordReset query()
 * @mixin Eloquent
 * @property int $id
 * @property string $email
 * @property string $token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|PasswordReset whereCreatedAt($value)
 * @method static Builder|PasswordReset whereEmail($value)
 * @method static Builder|PasswordReset whereId($value)
 * @method static Builder|PasswordReset whereToken($value)
 * @method static Builder|PasswordReset whereUpdatedAt($value)
 */
	class PasswordReset extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Store
 *
 * @property Collection|Tag[] $tags
 * @property-read User $user
 * @method static Builder|Store newModelQuery()
 * @method static Builder|Store newQuery()
 * @method static Builder|Store query()
 * @method static Builder|Store withAllTags($tags, $type = null)
 * @method static Builder|Store withAllTagsOfAnyType($tags)
 * @method static Builder|Store withAnyTags($tags, $type = null)
 * @method static Builder|Store withAnyTagsOfAnyType($tags)
 * @mixin Eloquent
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $address
 * @property string|null $image
 * @property string $latitude
 * @property string $longitude
 * @property int $user_id
 * @property int $active
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Store whereActive($value)
 * @method static Builder|Store whereAddress($value)
 * @method static Builder|Store whereCreatedAt($value)
 * @method static Builder|Store whereDescription($value)
 * @method static Builder|Store whereId($value)
 * @method static Builder|Store whereImage($value)
 * @method static Builder|Store whereLatitude($value)
 * @method static Builder|Store whereLongitude($value)
 * @method static Builder|Store whereName($value)
 * @method static Builder|Store whereUpdatedAt($value)
 * @method static Builder|Store whereUserId($value)
 * @property string|null $featured_image
 * @property int|null $featured_image_id
 * @property string $avatar
 * @property-read mixed $avatar_url
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @property-read int|null $tags_count
 * @method static \Illuminate\Database\Eloquent\Builder|Store whereAvatar($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Store whereFeaturedImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Store whereFeaturedImageId($value)
 */
	class Store extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\Team
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property bool $personal_team
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $owner
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\TeamInvitation[] $teamInvitations
 * @property-read int|null $team_invitations_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $users
 * @property-read int|null $users_count
 * @method static \Database\Factories\TeamFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Team newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Team newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Team query()
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team wherePersonalTeam($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Team whereUserId($value)
 */
	class Team extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\TeamInvitation
 *
 * @property int $id
 * @property int $team_id
 * @property string $email
 * @property string|null $role
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Team $team
 * @method static \Illuminate\Database\Eloquent\Builder|TeamInvitation newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TeamInvitation newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TeamInvitation query()
 * @method static \Illuminate\Database\Eloquent\Builder|TeamInvitation whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamInvitation whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamInvitation whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamInvitation whereRole($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamInvitation whereTeamId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TeamInvitation whereUpdatedAt($value)
 */
	class TeamInvitation extends \Eloquent {}
}

namespace App\Models{
/**
 * App\Models\User
 *
 * @property-read Collection|Client[] $clients
 * @property-read Collection|Deal[] $deals
 * @property-read Collection|Deal[] $favourites
 * @property-read Collection|Deal[] $grabs
 * @property-read Collection|Notification[] $notifications
 * @property-read Collection|Deal[] $notified
 * @property-read Collection|Deal[] $smashes
 * @property-read Collection|Store[] $stores
 * @property-read Collection|Token[] $tokens
 * @method static bool|null forceDelete()
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static \Illuminate\Database\Query\Builder|User onlyTrashed()
 * @method static Builder|User query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|User withoutTrashed()
 * @mixin Eloquent
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $role
 * @property string|null $phone
 * @property string|null $city
 * @property Carbon|null $email_verified_at
 * @property string $avatar
 * @property string|null $address
 * @property string|null $description
 * @property string|null $settings
 * @property string|null $shortcuts
 * @property string $password
 * @property int $active
 * @property string|null $activation_token
 * @property int $allow_push_notifications
 * @property int $allow_email_notifications
 * @property string|null $expo_push_token
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @method static Builder|User whereActivationToken($value)
 * @method static Builder|User whereActive($value)
 * @method static Builder|User whereAddress($value)
 * @method static Builder|User whereAllowEmailNotifications($value)
 * @method static Builder|User whereAllowPushNotifications($value)
 * @method static Builder|User whereAvatar($value)
 * @method static Builder|User whereCity($value)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereDeletedAt($value)
 * @method static Builder|User whereDescription($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereExpoPushToken($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User wherePhone($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereRole($value)
 * @method static Builder|User whereSettings($value)
 * @method static Builder|User whereShortcuts($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @property string|null $featured_image
 * @property int|null $featured_image_id
 * @property string|null $two_factor_secret
 * @property string|null $two_factor_recovery_codes
 * @property-read int|null $clients_count
 * @property-read \App\Models\Team $currentTeam
 * @property-read int|null $deals_count
 * @property-read int|null $favourites_count
 * @property-read mixed $avatar_url
 * @property-read int|null $grabs_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @property-read int|null $notifications_count
 * @property-read int|null $notified_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Team[] $ownedTeams
 * @property-read int|null $owned_teams_count
 * @property-read int|null $smashes_count
 * @property-read int|null $stores_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Team[] $teams
 * @property-read int|null $teams_count
 * @property-read int|null $tokens_count
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFeaturedImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereFeaturedImageId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTwoFactorRecoveryCodes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|User whereTwoFactorSecret($value)
 */
	class User extends \Eloquent {}
}

