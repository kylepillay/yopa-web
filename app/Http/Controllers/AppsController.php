<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Deal;
use App\Models\Store;
use App\Models\User;

class AppsController extends Controller
{
    // User List Page
    public function user_list()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/user/app-user-list', ['pageConfigs' => $pageConfigs]);
    }

    // User Account Page
    public function user_edit(User $user)
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/user/app-user-edit', ['pageConfigs' => $pageConfigs, 'user' => $user, 'add' => false]);
    }

    public function user_add()
    {
        $pageConfigs = ['pageHeader' => false];
        $user = new User();
        return view('/content/apps/user/app-user-edit', ['pageConfigs' => $pageConfigs, 'user' => $user, 'add' => true]);
    }

    public function deal_list()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/deals/app-deal-list', ['pageConfigs' => $pageConfigs]);
    }

    public function deal_edit(Deal $deal)
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/deals/app-deal-edit', ['pageConfigs' => $pageConfigs, 'deal' => $deal, 'add' => false]);
    }

    public function deal_add(Deal $deal)
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/deals/app-deal-edit', ['pageConfigs' => $pageConfigs, 'deal' => $deal, 'add' => true]);
    }

    public function category_list()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/categories/app-category-list', ['pageConfigs' => $pageConfigs]);
    }

    public function category_edit(Category $category)
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/categories/app-category-edit', ['pageConfigs' => $pageConfigs, 'category' => $category, 'add' => false]);
    }

    public function category_add(Category $category)
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/categories/app-category-edit', ['pageConfigs' => $pageConfigs, 'category' => $category, 'add' => true]);
    }

    public function store_list()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/stores/app-store-list', ['pageConfigs' => $pageConfigs]);
    }

    public function store_edit(Store $store)
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/stores/app-store-edit', ['pageConfigs' => $pageConfigs, 'store' => $store, 'add' => false]);
    }

    public function store_add(Store $store)
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/stores/app-store-edit', ['pageConfigs' => $pageConfigs, 'store' => $store, 'add' => true]);
    }

    // User Security Page
    public function user_view_security()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/user/app-user-view-security', ['pageConfigs' => $pageConfigs]);
    }

    // User Billing and Plans Page
    public function user_view_billing()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/user/app-user-view-billing', ['pageConfigs' => $pageConfigs]);
    }

    // User Notification Page
    public function user_view_notifications()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/user/app-user-view-notifications', ['pageConfigs' => $pageConfigs]);
    }

    // User Connections Page
    public function user_view_connections()
    {
        $pageConfigs = ['pageHeader' => false];
        return view('/content/apps/user/app-user-view-connections', ['pageConfigs' => $pageConfigs]);
    }


    // Chat App
    public function chatApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'contentLayout' => "content-left-sidebar",
            'pageClass' => 'chat-application',
        ];

        return view('/content/apps/chat/app-chat', [
            'pageConfigs' => $pageConfigs
        ]);
    }

    // Calender App
    public function calendarApp()
    {
        $pageConfigs = [
            'pageHeader' => false
        ];

        return view('/content/apps/calendar/app-calendar', [
            'pageConfigs' => $pageConfigs
        ]);
    }

    // Email App
    public function emailApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'contentLayout' => "content-left-sidebar",
            'pageClass' => 'email-application',
        ];

        return view('/content/apps/email/app-email', ['pageConfigs' => $pageConfigs]);
    }
    // ToDo App
    public function todoApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'contentLayout' => "content-left-sidebar",
            'pageClass' => 'todo-application',
        ];

        return view('/content/apps/todo/app-todo', [
            'pageConfigs' => $pageConfigs
        ]);
    }
    // File manager App
    public function file_manager()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'contentLayout' => "content-left-sidebar",
            'pageClass' => 'file-manager-application',
        ];

        return view('/content/apps/fileManager/app-file-manager', ['pageConfigs' => $pageConfigs]);
    }

    // Access Roles App
    public function access_roles()
    {
        $pageConfigs = ['pageHeader' => false,];

        return view('/content/apps/rolesPermission/app-access-roles', ['pageConfigs' => $pageConfigs]);
    }

    // Access permission App
    public function access_permission()
    {
        $pageConfigs = ['pageHeader' => false,];

        return view('/content/apps/rolesPermission/app-access-permission', ['pageConfigs' => $pageConfigs]);
    }

    // Kanban App
    public function kanbanApp()
    {
        $pageConfigs = [
            'pageHeader' => false,
            'pageClass' => 'kanban-application',
        ];

        return view('/content/apps/kanban/app-kanban', ['pageConfigs' => $pageConfigs]);
    }

    // Ecommerce Shop
    public function ecommerce_shop()
    {
        $pageConfigs = [
            'contentLayout' => "content-detached-left-sidebar",
            'pageClass' => 'ecommerce-application',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['name' => "Shop"]
        ];

        return view('/content/apps/ecommerce/app-ecommerce-shop', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    // Ecommerce Details
    public function ecommerce_details()
    {
        $pageConfigs = [
            'pageClass' => 'ecommerce-application',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['link' => "/app/ecommerce/shop", 'name' => "Shop"], ['name' => "Details"]
        ];

        return view('/content/apps/ecommerce/app-ecommerce-details', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    // Ecommerce Wish List
    public function ecommerce_wishlist()
    {
        $pageConfigs = [
            'pageClass' => 'ecommerce-application',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['name' => "Wish List"]
        ];

        return view('/content/apps/ecommerce/app-ecommerce-wishlist', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }

    // Ecommerce Checkout
    public function ecommerce_checkout()
    {
        $pageConfigs = [
            'pageClass' => 'ecommerce-application',
        ];

        $breadcrumbs = [
            ['link' => "/", 'name' => "Home"], ['link' => "javascript:void(0)", 'name' => "eCommerce"], ['name' => "Checkout"]
        ];

        return view('/content/apps/ecommerce/app-ecommerce-checkout', [
            'pageConfigs' => $pageConfigs,
            'breadcrumbs' => $breadcrumbs
        ]);
    }
}
