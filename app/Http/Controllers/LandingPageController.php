<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LandingPageController extends Controller
{
  // Dashboard - Analytics
  public function index()
  {

    return view('landing');
  }
}
