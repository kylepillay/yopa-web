<?php

namespace App\Http\Controllers\API\Mobile;

use App\Models\Deal;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use function response;

class FavouritesController extends Controller
{

    public function index() {

        $deals = Auth::user()->favourites()->get();
        $total = Auth::user()->favourites()->count();

        return response()->json(['favourites' => $deals, 'total' => $total], 200);
    }

    public function create(Request $request): JsonResponse
    {

        $dealId = $request->get('deal_id');

        Auth::user()->favourites()->toggle($dealId);
        $deal = Deal::find($dealId);

        return response()->json(['status' => 'success', 'deal' => $deal], 201);
    }
}
