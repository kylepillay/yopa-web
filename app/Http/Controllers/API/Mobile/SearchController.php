<?php

namespace App\Http\Controllers\API\Mobile;

use App\Models\Deal;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function searchByLocation(Request $request): JsonResponse
    {

        $latitude = $request->get('latitude');
        $longitude = $request->get('longitude');
        $radius = $request->get('radius');
        $maxPrice = $request->get('price');
        $province = $request->get('province_id');

        $deals = Deal::select('deals.*')
            ->selectRaw('( 3959 * acos( cos( radians(?) ) *
                           cos( radians( latitude ) )
                           * cos( radians( longitude ) - radians(?)
                           ) + sin( radians(?) ) *
                           sin( radians( latitude ) ) )
                         ) AS distance', [$latitude, $longitude, $latitude])
            ->havingRaw("distance < ?", [$radius])
            ->where('salePrice', '<', $maxPrice)
            ->where('province_id',  $province )
            ->get();

        return response()->json(['deals' => $deals, 'total' => sizeof($deals)], 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function searchByProvince(Request $request): JsonResponse
    {

        $deals = Deal::where('province_id', $request->get('province_id'))->get();

        return response()->json(['status' => 'success', 'deals' => $deals, 'total' => sizeof($deals)], 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function searchByCategories(Request $request): JsonResponse
    {

        $categories = $request->get('categories');

        $deals = Deal::whereIn('category_id', $categories)->get();

        return response()->json(['status' => 'success', 'deals' => $deals, 'total' => sizeof($deals)], 200);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function searchByText(Request $request): JsonResponse
    {

        $deals = Deal::where('name','LIKE','%'.$request->get('search_text').'%')->orWhere('description','LIKE','%'.$request->get('search_text').'%')->get();

        return response()->json(['status' => 'success', 'deals' => $deals, 'total' => sizeof($deals)], 200);
    }
}
