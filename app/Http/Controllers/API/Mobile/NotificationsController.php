<?php

namespace App\Http\Controllers\API\Mobile;

use App\Models\Deal;
use App\Models\Notification;
use App\Models\User;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class NotificationsController extends Controller
{

    public function index(Request $request): JsonResponse
    {

        $notifications = Auth::user()->notifications()->with(['deal', 'user'])->get();

        return response()->json(['status' => 'success', 'notifications' => $notifications], 200);
    }


    public function check(Request $request): JsonResponse
    {

        $latitude = $request->get('latitude');
        $longitude = $request->get('longitude');
        $radius = 5;

        $deals = Deal::select('deals.*')
            ->selectRaw('( 3959 * acos( cos( radians(?) ) *
                           cos( radians( latitude ) )
                           * cos( radians( longitude ) - radians(?)
                           ) + sin( radians(?) ) *
                           sin( radians( latitude ) ) )
                         ) AS distance', [$latitude, $longitude, $latitude])
            ->havingRaw("distance < ?", [$radius])
            ->get();

        if (sizeof($deals) < 1) {
            return response()->json(['status' => 'no-deals'], 201);
        }

        $result = Auth::user()->notified()->sync([ $deals[0]->id], false);

        if (sizeof($result['attached'] ) > 0) {
            $user = Auth::user();

            $notification = new Notification();
            $notification->deal_id = $deals[0]->id;
            $notification->user_id = $user->id;
            $notification->save();

            $token = $user->expo_push_token;

            $client = new Client();

            $client->post(
                'https://exp.host/--/api/v2/push/send',
                [
                    'form_params' => [
                        'to' => $token,
                        'title' => 'Deal Nearby',
                        'body' => 'There is a new deal in your vicinity!',
                        'data' => [
                            'title' => 'Deal Nearby',
                            'deal_id' => $deals[0]->id
                        ]
                    ]
                ]
            );

            return response()->json(['status' => 'success', 'message' => 'Notification Added'], 201);
        }

        return response()->json(['status' => 'no-attached'], 201);
    }


    public function remove(Request $request): JsonResponse
    {

        Notification::destroy([$request->get('notification_id')]);

        return response()->json(['status' => 'success'], 200);
    }

    public function registerPush(Request $request): JsonResponse
    {

        $user = Auth::user();
        $user->expo_push_token = $request->get('token');
        $user->save();

        return response()->json(['user' => $user], 200);
    }

    public function sendPush(Request $request): JsonResponse
    {

        $user = User::where('id', $request->get('user_id'))->first();
        $token = $user->expo_push_token;

        $client = new Client();

        $client->post(
            'https://exp.host/--/api/v2/push/send',
            [
                'form_params' => [
                    'to' => $token,
                    'title' => $request->get('title'),
                    'body' => $request->get('body'),
                    'data' => [
                        'title' => $request->get('title'),
                        'deal_id' => $request->get('deal_id')
                    ]
                ]
            ]
        );

        return response()->json(['user' => $user], 200);
    }

}
