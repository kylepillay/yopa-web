<?php

namespace App\Http\Controllers\API\Mobile;

use App\Models\Category;
use App\Models\Deal;
use App\Models\Province;
use Auth;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $deals = Deal::all();

        return response()->json(['deals' => $deals], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param Deal $deal
     * @return JsonResponse
     */
    public function show(Deal $deal): JsonResponse
    {
        return response()->json([
            'deal' => $deal
        ], 200);
    }

    public function smash(Request $request): JsonResponse
    {

        $dealId = $request->get('deal_id');
        $deal = Deal::find($dealId);

        $result = Auth::user()->smashes()->sync([$dealId], false);

        return response()->json(['status' => 'success', 'result' => $result, 'deal' => $deal], 201);
    }

    public function grab(Request $request): JsonResponse
    {

        $result = Auth::user()->grabs()->sync([$request->get('deal_id')], false);

        return response()->json(['status' => 'success', 'result' => $result], 201);
    }

    public function allProvinces(): JsonResponse
    {
        $provinces = Province::all();
        return response()->json($provinces);
    }


    public function categories(Request $request): JsonResponse
    {

        $categories = Category::all();

        return response()->json(['status' => 'success', 'categories' => $categories], 200);
    }


}
