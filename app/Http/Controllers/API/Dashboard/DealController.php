<?php

namespace App\Http\Controllers\API\Dashboard;

use App\Models\Deal;
use App\Models\Image;
use App\Models\Province;
use App\Models\Store;
use Avatar;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Storage;
use Str;

class DealController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        if (Auth::check()) {
            if (Auth::user()->role === 'admin') {
                return response()->json(Deal::all(), 200);
            } else {
                return response()->json(Auth::user()->deals()->get(), 200);
            }
        }

        return response()->json(['message' => 'Unauthorized.'], 401);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $postInput = file_get_contents('php://input');
        $data = json_decode($postInput, true);

        if (Auth::check()) {

            $store = Store::where('id', $data['store_id'])->first();

            $data['user_id'] = Auth::user()->id;
            $data['latitude'] = $store->latitude;
            $data['longitude'] = $store->longitude;
            $data['province'] = $store->province;

            $deal = Deal::create($data);

            $avatar = Avatar::create($deal->name)->getImageObject()->encode('png');
            Storage::put('public/deal-avatars/'.$deal->id.'/avatar.png', (string) $avatar);

            return response()->json($deal, 201);
        }

        return response()->json(['message' => 'Unauthorized.'], 401);
    }

    /**
     * Display the specified resource.
     *
     * @param Deal $deal
     * @return JsonResponse
     */
    public function show(Deal $deal): JsonResponse
    {
        return response()->json($deal, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Deal $deal
     * @return JsonResponse
     */
    public function update(Request $request, Deal $deal): JsonResponse
    {
        $postInput = file_get_contents('php://input');
        $data = json_decode($postInput, true);


        $deal->update($data);
        $deal->syncTags($data['tags']);

        return response()->json($deal, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Deal $deal
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Deal $deal): JsonResponse
    {
        $deal->delete();

        return response()->json(null, 204);
    }

    public function uploadImages(Request $request)
    {
        $file = $request->file('file');
        $ext = $file->extension();
        $name = Str::random(20).'.'.$ext ;
        list($width, $height) = getimagesize($file);
        $path = Storage::disk('public')->putFileAs(
            'uploads', $file, $name
        );
        if($path){
            $deal = Deal::find($request->get('entity'));
            $create = $deal->images()->create([
                'uri' => $path,
                'public' => false,
                'height' => $height,
                'width' => $width
            ]);

            if($create){
                return response()->json([
                    'uploaded' => true
                ]);
            }
        }
    }

    public function getImages(Request $request): JsonResponse
    {
        $deal = Deal::find($request->get('entity'));
        return response()->json($deal->images()->get()->toArray());
    }

    public function deleteImages(Request $request)
    {
        $image = Image::find($request->id);
        if(Storage::disk('public')->delete($image->uri) && $image->delete()){
            return response()->json([
                'deleted' => true
            ]);
        }
    }

    public function setFeaturedImage(Request $request): JsonResponse
    {
        $image = Image::find($request->get('id'));
        $deal = Deal::find($request->get('entity'));
        $deal->featured_image = Storage::url($image->uri);
        $deal->featured_image_id = $image->id;
        $deal->save();

        return response()->json([
            'message' => 'Successfully updated featured image!',
            'deal' => $deal
        ], 201);
    }
}
