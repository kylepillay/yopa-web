<?php

namespace App\Http\Controllers\API\Dashboard;

use App\Models\Image;
use App\Models\Store;
use Avatar;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Response;
use Storage;
use Str;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function __invoke(): JsonResponse
    {
        if (Auth::check()) {
            if (Auth::user()->role === 'admin') {
                return response()->json(Store::all(), 200);
            } else {
                return response()->json(Auth::user()->stores()->get(), 200);
            }
        }

        return response()->json([], 401);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        if (Auth::check()) {
            $postInput = file_get_contents('php://input');
            $data = json_decode($postInput, true);

            $data['user_id'] = Auth::user()->id;

            $store = Store::create($data);

            $avatar = Avatar::create($store->name)->getImageObject()->encode('png');
            Storage::put('public/store-avatars/'.$store->id.'/avatar.png', (string) $avatar);

            return response()->json($store, 201);
        }

        return response()->json(['message' => 'Unauthorized.'], 401);
    }

    /**
     * Display the specified resource.
     *
     * @param Store $store
     * @return JsonResponse
     */
    public function show(Store $store)
    {
        return response()->json($store, 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Store $store
     * @return JsonResponse
     */
    public function update(Request $request, Store $store)
    {
        $postInput = file_get_contents('php://input');
        $data = json_decode($postInput, true);

        $store->update($data);

        return response()->json($store, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Store $store
     * @return Response
     * @throws Exception
     */
    public function destroy(Store $store)
    {
        $store->delete();

        return response()->json(null, 204);
    }

    public function uploadImages(Request $request)
    {
        $file = $request->file('file');
        $ext = $file->extension();
        $name = Str::random(20).'.'.$ext ;
        list($width, $height) = getimagesize($file);
        $path = Storage::disk('public')->putFileAs(
            'uploads', $file, $name
        );
        if($path){
            $store = Store::find($request->get('entity'));
            $create = $store->images()->create([
                'uri' => $path,
                'public' => false,
                'height' => $height,
                'width' => $width
            ]);

            if($create){
                return response()->json([
                    'uploaded' => true
                ]);
            }
        }
    }

    public function getImages(Request $request): JsonResponse
    {
        $store = Store::find($request->get('entity'));
        return response()->json($store->images()->get()->toArray());
    }

    public function deleteImages(Request $request)
    {
        $image = Image::find($request->id);
        if(Storage::disk('public')->delete($image->uri) && $image->delete()){
            return response()->json([
                'deleted' => true
            ]);
        }
    }

    public function setFeaturedImage(Request $request)
    {
        $image = Image::find($request->get('id'));
        $store = Store::find($request->get('entity'));
        $store->featured_image = Storage::url($image->uri);
        $store->featured_image_id = $image->id;
        $store->save();

        return response()->json([
            'message' => 'Successfully updated featured image!',
            'store' => $store
        ], 201);
    }
}
