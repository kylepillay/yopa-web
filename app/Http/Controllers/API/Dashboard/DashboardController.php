<?php

namespace App\Http\Controllers\API\Dashboard;

use App\Models\Store;
use App\Http\Controllers\Controller;
use App\Models\Deal;
use Auth;

class DashboardController extends Controller
{
    public function index() {

        if (Auth::check()) {

            $userId = Auth::user()->id;
            $totalDeals = Deal::where('user_id', $userId)->count();
            $totalStores = Store::where('user_id', $userId)->count();
            $activeDeals = Deal::where('active', 1)->where('user_id', $userId)->get()->count();
            $activeStores = Store::where('active', 1)->where('user_id',$userId)->get()->count();

            $response = [
                'widget1' => [
                    'title' => 'Deals',
                    'data' => [
                        'label' => 'Total Deals',
                        'count' => $totalDeals,
                        'extra' => [
                            'label' => 'Active Deals',
                            'count' => $activeDeals,
                        ]
                    ]
                ],
                'widget2' => [
                    'title' => 'Stores',
                    'data' => [
                        'label' => 'Total Stores',
                        'count' => $totalStores,
                        'extra' => [
                            'label' => 'Active Stores',
                            'count' => $activeStores,
                        ]
                    ]
                ],
            ];

            return response()->json($response, 200);
        }

        $response = [
            'widget1' => [
                'title' => 'Deals',
                'data' => [
                    'label' => 'Total Deals',
                    'count' => 0,
                    'extra' => [
                        'label' => 'Active Deals',
                        'count' => 0,
                    ]
                ]
            ],
            'widget2' => [
                'title' => 'Stores',
                'data' => [
                    'label' => 'Total Stores',
                    'count' => 0,
                    'extra' => [
                        'label' => 'Active Stores',
                        'count' => 0,
                    ]
                ]
            ],
        ];

        return response()->json($response, 200);
    }
}
