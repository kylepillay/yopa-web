<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Faker\Factory as Faker;
use Hash;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Auth;
use App\Traits\ApiResponser;
use App\Models\User;
use Illuminate\Validation\ValidationException;
use Mail;
use Spatie\MediaLibrary\MediaCollections\Exceptions\FileCannotBeAdded;
use Spatie\MediaLibrary\MediaCollections\Exceptions\InvalidBase64Data;
use Str;
use function url;


class AuthController extends Controller
{
    use ApiResponser;

    /**
     * Create user
     *
     * @param Request $request
     * @return JsonResponse [string] message
     */
    public function register(Request $request): JsonResponse
    {

        $request->validate([
            'name' => 'required|min:4|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|min:4|confirmed',
            'device_name' => 'string'
        ]);

        $user = new User([
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'role' => $request->get('role'),
            'password' => bcrypt($request->get('password')),
            'activation_token' => Str::random(60)
        ]);

        $user->save();

        $faker = Faker::create();
        $imageUrl = $faker->imageUrl(640, 480, null, false);
        $user->addMediaFromUrl($imageUrl)->withResponsiveImages()->toMediaCollection('images');

        $url = url('/api/auth/register/activate/'.$user->activation_token);

        Mail::send('emails.activate', ['token' => $url], function ($message) use ($user) {
            $message->to($user->email)->subject('YoPa - Profile Activation');
        });

        return $this->success([
            'token' => $user->createToken($request->get('device_name') ?? 'Mobile User Token')->plainTextToken,
            'message' => 'Successfully created user!'
        ]);
    }

    public function registerActivate($token)
    {
        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            return response()->json([
                'message' => 'This activation token is invalid.'
            ], 404);
        }

        $user->active = true;
        $user->activation_token = '';
        $user->save();

        return view('success');
    }

    /**
     * Update user
     *
     * @param Request $request
     * @return JsonResponse [string] message
     */
    public function update(Request $request): JsonResponse
    {
        $user = Auth::user();

        $request->validate([
            'password' => 'confirmed',
        ]);

        $password = $request->get('password');
        $password_confirmation = $request->get('password_confirmation');

        $data = [
            "name" => $request->get('name'),
            "email" => $request->get('email'),
            "phone" => $request->get('phone'),
            "city" => $request->get('city'),
            "allow_push_notifications" => $request->get('allow_push_notifications'),
            "allow_email_notifications" => $request->get('allow_email_notifications'),
        ];

        if ($password === $password_confirmation) {
            $data["password"] = bcrypt($password);
        }

        $user->update($data);

        return $this->success([
            'message' => 'Successfully updated user!', 'user' => $user
        ]);
    }

    /**
     * Login user and create token
     *
     * @param Request $request
     * @return JsonResponse [string] access_token
     */
    public function login(Request $request): JsonResponse
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'device_name' => 'string'
        ]);

        $user = User::where('email', $request->email)->first();

        if (!$user || ! Hash::check($request->password, $user->password)) {
            throw ValidationException::withMessages([
                'email' => ['The provided credentials are incorrect.'],
            ]);
        }

        return $this->success([
            'access_token' => $user->createToken('API Token')->plainTextToken,
            'user' => $user
        ]);
    }

    /**
     * Logout user (Revoke the token)
     *
     * @param Request $request
     * @return JsonResponse [string] message
     */
    public function logout(Request $request): JsonResponse
    {
        $request->user()->tokens()->delete();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Get the authenticated User
     *
     * @param Request $request
     * @return JsonResponse [json] user object
     */
    public function user(Request $request): JsonResponse
    {
        return response()->json($request->user());
    }
}
