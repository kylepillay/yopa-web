<?php

namespace App\Http\Controllers;

use App\Models\Store;
use App\Models\Deal;
use Auth;

class DashboardController extends Controller
{
    public function index()
    {
        $userId = Auth::user()->id;

        $dashboardData = [
            'user_id' => $userId,
            'total_deals' => $userId === 1 ? Deal::all()->count() : Deal::where('user_id', $userId)->count(),
            'total_stores' => $userId === 1 ? Deal::all()->count() : Store::where('user_id', $userId)->count(),
            'active_deals' => $userId === 1 ? Deal::where('active', 1)->count() : Deal::where('active', 1)->where('user_id', $userId)->get()->count(),
            'active_stores' => $userId === 1 ? Store::where('active', 1)->count() : Store::where('active', 1)->where('user_id', $userId)->get()->count()
        ];
        $pageConfigs = ['pageHeader' => false];

        return view('content.dashboard.dashboard-ecommerce', ['pageConfigs' => $pageConfigs, 'dashboardData' => $dashboardData]);
    }
}
