<?php

namespace App\Http\Livewire\Category;

use App\Models\Category;
use Livewire\Component;
use Livewire\WithPagination;

class All extends Component
{
    use WithPagination;

    public int $perPage;

    public function mount() {
        $this->perPage = 10;
    }

    public function render()
    {
        $categories = Category::paginate($this->perPage);

        return view('livewire.category.all', [
            'categories' => $categories,
        ]);
    }
}
