<?php

namespace App\Http\Livewire\Category;

use App\Models\Category;
use Livewire\Component;

class Show extends Component
{
    public Category $category;
    public bool $add;

    protected array $rules = [
      'category.name' => 'string|required'
    ];

    public function render()
    {
        return view('livewire.category.show');
    }

    public function save()
    {
        $this->validate();
        $this->category->save();
    }
}
