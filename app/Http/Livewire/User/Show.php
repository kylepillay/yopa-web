<?php

namespace App\Http\Livewire\User;

use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Livewire\Component;

class Show extends Component
{
    public User $user;
    public bool $add;

    protected array $rules = [
        'user.name' => 'required|string',
        'user.email' => 'required|string',
        'user.role' => 'required|string',
        'user.active' => 'boolean',
        'user.phone' => 'required|string',
        'user.city' => 'required|string',
        'user.password' => 'required',
    ];

    public function render()
    {
        return view('livewire.user.show');
    }

    public function save(): RedirectResponse
    {
        $this->validate();
        $this->user->save();
        session()->flash('message', 'User successfully '.$this->add ? 'created.' : 'updated.');
        return redirect()->to('/admin/users/'.$this->user->id);
    }
}
