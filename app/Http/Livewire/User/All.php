<?php

namespace App\Http\Livewire\User;

use App\Models\User;
use Livewire\Component;
use Livewire\WithPagination;

class All extends Component
{
    use WithPagination;

    public int $perPage;

    public function mount() {
        $this->perPage = 10;
    }

    public function render()
    {
        return view('livewire.user.all', [
            'users' => User::paginate($this->perPage),
        ]);
    }
}
