<?php

namespace App\Http\Livewire\Store;

use App\Models\Store;
use Livewire\Component;
use Livewire\WithPagination;
use Auth;

class All extends Component
{
    use WithPagination;

    public int $perPage;

    public function mount() {
        $this->perPage = 10;
    }

    public function render()
    {
        $stores = Store::paginate($this->perPage);

        if (Auth::check()) {
            if (Auth::user()->role === 'staff') {
                $stores = Auth::user()->stores()->paginate($this->perPage);
            }
        }

        return view('livewire.store.all', [
            'stores' => $stores,
        ]);
    }
}
