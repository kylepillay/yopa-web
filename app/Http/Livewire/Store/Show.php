<?php

namespace App\Http\Livewire\Store;

use App\Models\Store;
use App\Models\User;
use Livewire\Component;
use Livewire\WithFileUploads;
use Spatie\MediaLibraryPro\Http\Livewire\Concerns\WithMedia;

class Show extends Component
{
    use WithFileUploads, WithMedia;

    public Store $store;
    public bool $add;
    public $mediaComponentNames = ['photo'];
    public $photo;
    public $users;

    protected array $rules = [
        'store.name' => 'required|string',
        'store.address' => 'required|string',
        'store.description' => 'required|string',
        'store.latitude' => 'required|string',
        'store.longitude' => 'required|string',
        'store.active' => 'boolean',
        'store.user_id' => 'int|required'
    ];

    public function mount()
    {
        $this->users = User::all();
    }

    public function render()
    {
        return view('livewire.store.show');
    }

    public function save()
    {
        $this->validate();
        $this->store->save();
        if ($this->photo) {
            $this->store->clearMediaCollection('images');
            $this->store->addFromMediaLibraryRequest($this->photo)->toMediaCollection('images');
            $this->clearMedia();
        }
    }
}
