<?php

namespace App\Http\Livewire\Deal;

use App\Models\Deal;
use App\Models\Store;
use App\Models\User;
use DealCategory;
use Livewire\Component;
use Livewire\WithFileUploads;
use Spatie\MediaLibraryPro\Http\Livewire\Concerns\WithMedia;

class Show extends Component
{
    use WithFileUploads, WithMedia;

    public Deal $deal;
    public bool $add;
    public $mediaComponentNames = ['photo'];
    public $photo;
    public $categories;
    public $users;
    public $stores;

    protected array $rules = [
        'deal.name' => 'required|string',
        'deal.handle' => 'required|string',
        'deal.description' => 'required|string',
        'deal.link' => 'required|string',
        'deal.price' => 'required|string',
        'deal.salePrice' => 'required|string',
        'deal.latitude' => 'required|string',
        'deal.longitude' => 'required|string',
        'deal.active' => 'boolean',
        'deal.category_id' => 'int|required',
        'deal.user_id' => 'int|required',
        'deal.store_id' => 'int|required',
    ];

    public function mount()
    {
        $this->categories = Deal::all();
        $this->users = User::all();
        $this->stores = Store::all();
    }

    public function render()
    {
        return view('livewire.deal.show');
    }

    public function save()
    {
        $this->validate();
        $this->deal->save();
        if ($this->photo) {
            $this->deal->clearMediaCollection('images');
            $this->deal->addFromMediaLibraryRequest($this->photo)->toMediaCollection('images');
            $this->clearMedia();
        }
    }
}
