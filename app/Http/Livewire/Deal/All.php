<?php

namespace App\Http\Livewire\Deal;

use App\Models\Deal;
use Livewire\Component;
use Livewire\WithPagination;
use Auth;

class All extends Component
{
    use WithPagination;

    public int $perPage;

    public function mount() {
        $this->perPage = 10;
    }

    public function render()
    {
        $deals = Deal::paginate($this->perPage);

        if (Auth::check()) {
            if (Auth::user()->role === 'staff') {
                $deals = Auth::user()->deals()->paginate($this->perPage);
            }
        }

        return view('livewire.deal.all', [
            'deals' => $deals,
        ]);
    }
}
