<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Notification
 *
 * @property-read Deal $deal
 * @property-read User $user
 * @method static Builder|Notification newModelQuery()
 * @method static Builder|Notification newQuery()
 * @method static Builder|Notification query()
 * @mixin Eloquent
 * @property int $id
 * @property int $user_id
 * @property int $deal_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Notification whereCreatedAt($value)
 * @method static Builder|Notification whereDealId($value)
 * @method static Builder|Notification whereId($value)
 * @method static Builder|Notification whereUpdatedAt($value)
 * @method static Builder|Notification whereUserId($value)
 */
class Notification extends Model
{
    protected $fillable = ['user_id', 'deal_id'];

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function deal()
    {
        return $this->belongsTo('App\Models\Deal', 'deal_id');
    }
}
