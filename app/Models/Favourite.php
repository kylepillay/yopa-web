<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * App\Models\Favourite
 *
 * @method static Builder|Favourite newModelQuery()
 * @method static Builder|Favourite newQuery()
 * @method static Builder|Favourite query()
 * @mixin Eloquent
 * @property int $user_id
 * @property int $deal_id
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Favourite whereCreatedAt($value)
 * @method static Builder|Favourite whereDealId($value)
 * @method static Builder|Favourite whereUpdatedAt($value)
 * @method static Builder|Favourite whereUserId($value)
 */
class Favourite extends Model
{
    protected $fillable = ['user_id', 'deal_id'];
}
