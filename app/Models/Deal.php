<?php

namespace App\Models;

use Auth;
use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;
use Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection;
use Spatie\Tags\HasTags;
use Spatie\Tags\Tag;
use Storage;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

/**
 * App\Models\Deal
 *
 * @property-read Category $category
 * @property-read mixed $favourited
 * @property-read mixed $grabbed
 * @property-read mixed $smashed
 * @property Collection|Tag[] $tags
 * @property-read Store $store
 * @property-read User $user
 * @method static Builder|Deal newModelQuery()
 * @method static Builder|Deal newQuery()
 * @method static Builder|Deal query()
 * @method static Builder|Deal withAllTags($tags, $type = null)
 * @method static Builder|Deal withAllTagsOfAnyType($tags)
 * @method static Builder|Deal withAnyTags($tags, $type = null)
 * @method static Builder|Deal withAnyTagsOfAnyType($tags)
 * @mixin Eloquent
 * @property int $id
 * @property string $name
 * @property string $handle
 * @property string $description
 * @property string|null $image
 * @property int $category_id
 * @property float $price
 * @property float $salePrice
 * @property int $user_id
 * @property int $store_id
 * @property string|null $link
 * @property string $latitude
 * @property string $longitude
 * @property int $active
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Deal whereActive($value)
 * @method static Builder|Deal whereCategoryId($value)
 * @method static Builder|Deal whereCreatedAt($value)
 * @method static Builder|Deal whereDescription($value)
 * @method static Builder|Deal whereHandle($value)
 * @method static Builder|Deal whereId($value)
 * @method static Builder|Deal whereImage($value)
 * @method static Builder|Deal whereLatitude($value)
 * @method static Builder|Deal whereLink($value)
 * @method static Builder|Deal whereLongitude($value)
 * @method static Builder|Deal whereName($value)
 * @method static Builder|Deal wherePrice($value)
 * @method static Builder|Deal whereSalePrice($value)
 * @method static Builder|Deal whereStoreId($value)
 * @method static Builder|Deal whereUpdatedAt($value)
 * @method static Builder|Deal whereUserId($value)
 * @property string|null $featured_image
 * @property int|null $featured_image_id
 * @property string $avatar
 * @property-read mixed $avatar_url
 * @property-read mixed $categories
 * @property-read mixed $stores
 * @property-read Collection|Image[] $images
 * @property-read int|null $images_count
 * @property-read MediaCollection|Media[] $media
 * @property-read int|null $media_count
 * @property-read int|null $tags_count
 * @method static Builder|Deal whereAvatar($value)
 * @method static Builder|Deal whereFeaturedImage($value)
 * @method static Builder|Deal whereFeaturedImageId($value)
 * @property string $province_id
 * @property-read string $imageurl
 * @property-read mixed $province
 * @method static Builder|Deal whereProvinceId($value)
 */
class Deal extends Model implements HasMedia
{
    use HasTags, InteractsWithMedia;

    protected $fillable = ['name', 'handle', 'description', 'image', 'category_id', 'price', 'salePrice', 'link', 'user_id', 'store_id', 'latitude', 'longitude', 'active', 'image'];
    protected $appends = ['favourited', 'smashed', 'grabbed', 'store', 'tags', 'category', 'categories', 'stores', 'avatar_url', 'imageurl'];

    public function getAvatarUrlAttribute(): string
    {
        return Storage::url('deal-avatars/'.$this->id.'/'.$this->avatar);
    }

    public function user(): BelongsTo
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function getStoreAttribute()
    {
        return $this->belongsTo('App\Models\Store', 'store_id')->first();
    }

    public function getProvinceAttribute()
    {
        return $this->belongsTo('App\Models\Province', 'province_id')->first();
    }

    public function getStoresAttribute(): Collection
    {
        return Auth::user()->stores()->get();
    }

    public function getTagsAttribute(): array
    {
        return $this->tags()->pluck('name')->toArray();
    }

    public function getCategoryAttribute()
    {
        return $this->belongsTo('App\Models\Category', 'category_id')->first();
    }

    public function getCategoriesAttribute()
    {
        return Category::all();
    }

    public function getFavouritedAttribute(): bool
    {
        return Auth::user()->favourites()->where('id', $this->id)->exists();
    }

    public function getImageurlAttribute(): string
    {
        return $this->getFirstMedia('images') ? $this->getFirstMedia('images')->getUrl() : '#';
    }

    public function getSmashedAttribute(): bool
    {
        return Auth::user()->smashes()->where('id', $this->id)->exists();
    }

    public function getGrabbedAttribute(): bool
    {
        return Auth::user()->grabs()->where('id', $this->id)->exists();
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')->width(200);
    }
}
