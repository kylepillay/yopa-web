<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Spatie\Tags\HasTags;
use Spatie\Tags\Tag;
use Storage;

/**
 * App\Models\Store
 *
 * @property Collection|Tag[] $tags
 * @property-read User $user
 * @method static Builder|Store newModelQuery()
 * @method static Builder|Store newQuery()
 * @method static Builder|Store query()
 * @method static Builder|Store withAllTags($tags, $type = null)
 * @method static Builder|Store withAllTagsOfAnyType($tags)
 * @method static Builder|Store withAnyTags($tags, $type = null)
 * @method static Builder|Store withAnyTagsOfAnyType($tags)
 * @mixin Eloquent
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $address
 * @property string|null $image
 * @property string $latitude
 * @property string $longitude
 * @property int $user_id
 * @property int $active
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @method static Builder|Store whereActive($value)
 * @method static Builder|Store whereAddress($value)
 * @method static Builder|Store whereCreatedAt($value)
 * @method static Builder|Store whereDescription($value)
 * @method static Builder|Store whereId($value)
 * @method static Builder|Store whereImage($value)
 * @method static Builder|Store whereLatitude($value)
 * @method static Builder|Store whereLongitude($value)
 * @method static Builder|Store whereName($value)
 * @method static Builder|Store whereUpdatedAt($value)
 * @method static Builder|Store whereUserId($value)
 * @property string|null $featured_image
 * @property int|null $featured_image_id
 * @property string $avatar
 * @property-read mixed $avatar_url
 * @property-read Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @property-read int|null $tags_count
 * @method static Builder|Store whereAvatar($value)
 * @method static Builder|Store whereFeaturedImage($value)
 * @method static Builder|Store whereFeaturedImageId($value)
 * @property int $province_id
 * @property-read string $imageurl
 * @property-read \Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection|Media[] $media
 * @property-read int|null $media_count
 * @property-read \App\Models\Province $province
 * @method static Builder|Store whereProvinceId($value)
 */
class Store extends Model implements HasMedia
{
    use HasTags, InteractsWithMedia;

    protected $fillable = ['name', 'description', 'image', 'address', 'user_id', 'latitude', 'longitude', 'active'];

    protected $appends = ['avatar_url', 'imageurl'];

    public function getAvatarUrlAttribute()
    {
        return Storage::url('store-avatars/'.$this->id.'/'.$this->avatar);
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id');
    }

    public function province()
    {
        return $this->belongsTo('App\Models\Province', 'province_id');
    }

    public function images() {
        return $this->morphMany('App\Models\Image', 'imageable');
    }

    public function getImageurlAttribute(): string
    {
        return $this->getFirstMedia('images') ? $this->getFirstMedia('images')->getUrl() : '#';
    }

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')->width(200);
    }
}
