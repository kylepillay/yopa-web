<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\Image
 *
 * @property int $id
 * @property int $imageable_id
 * @property string $imageable_type
 * @property string $uri
 * @property int $public
 * @property int $height
 * @property int $width
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read Model|Eloquent $imageable
 * @method static Builder|Image newModelQuery()
 * @method static Builder|Image newQuery()
 * @method static Builder|Image query()
 * @method static Builder|Image whereCreatedAt($value)
 * @method static Builder|Image whereHeight($value)
 * @method static Builder|Image whereId($value)
 * @method static Builder|Image whereImageableId($value)
 * @method static Builder|Image whereImageableType($value)
 * @method static Builder|Image wherePublic($value)
 * @method static Builder|Image whereUpdatedAt($value)
 * @method static Builder|Image whereUri($value)
 * @method static Builder|Image whereWidth($value)
 * @mixin Eloquent
 */
class Image extends Model
{
    protected $fillable = ['uri', 'public', 'height', 'width'];

    /**
     * Get all the owning imageable models.
     */
    public function imageable(): MorphTo
    {
        return $this->morphTo();
    }
}
