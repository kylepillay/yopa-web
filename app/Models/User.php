<?php

namespace App\Models;

use Eloquent;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Carbon;
use Laravel\Jetstream\HasProfilePhoto;
use Laravel\Jetstream\HasTeams;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Sanctum\HasApiTokens;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Collections\MediaCollection;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Storage;

/**
 * App\Models\User
 *
 * @property-read Collection|Client[] $clients
 * @property-read Collection|Deal[] $deals
 * @property-read Collection|Deal[] $favourites
 * @property-read Collection|Deal[] $grabs
 * @property-read Collection|Notification[] $notifications
 * @property-read Collection|Deal[] $notified
 * @property-read Collection|Deal[] $smashes
 * @property-read Collection|Store[] $stores
 * @property-read Collection|Token[] $tokens
 * @method static bool|null forceDelete()
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static \Illuminate\Database\Query\Builder|User onlyTrashed()
 * @method static Builder|User query()
 * @method static bool|null restore()
 * @method static \Illuminate\Database\Query\Builder|User withTrashed()
 * @method static \Illuminate\Database\Query\Builder|User withoutTrashed()
 * @mixin Eloquent
 * @property int $id
 * @property string $name
 * @property string $email
 * @property string $role
 * @property string|null $phone
 * @property string|null $city
 * @property Carbon|null $email_verified_at
 * @property string $avatar
 * @property string|null $address
 * @property string|null $description
 * @property string|null $settings
 * @property string|null $shortcuts
 * @property string $password
 * @property int $active
 * @property string|null $activation_token
 * @property int $allow_push_notifications
 * @property int $allow_email_notifications
 * @property string|null $expo_push_token
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property Carbon|null $deleted_at
 * @method static Builder|User whereActivationToken($value)
 * @method static Builder|User whereActive($value)
 * @method static Builder|User whereAddress($value)
 * @method static Builder|User whereAllowEmailNotifications($value)
 * @method static Builder|User whereAllowPushNotifications($value)
 * @method static Builder|User whereAvatar($value)
 * @method static Builder|User whereCity($value)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereDeletedAt($value)
 * @method static Builder|User whereDescription($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereExpoPushToken($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User wherePhone($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereRole($value)
 * @method static Builder|User whereSettings($value)
 * @method static Builder|User whereShortcuts($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @property string|null $featured_image
 * @property int|null $featured_image_id
 * @property string|null $two_factor_secret
 * @property string|null $two_factor_recovery_codes
 * @property-read int|null $clients_count
 * @property-read Team $currentTeam
 * @property-read int|null $deals_count
 * @property-read int|null $favourites_count
 * @property-read mixed $avatar_url
 * @property-read string $profile_photo_url
 * @property-read int|null $grabs_count
 * @property-read Collection|Image[] $images
 * @property-read int|null $images_count
 * @property-read int|null $notifications_count
 * @property-read int|null $notified_count
 * @property-read Collection|Team[] $ownedTeams
 * @property-read int|null $owned_teams_count
 * @property-read int|null $smashes_count
 * @property-read int|null $stores_count
 * @property-read Collection|Team[] $teams
 * @property-read int|null $teams_count
 * @property-read int|null $tokens_count
 * @method static Builder|User whereFeaturedImage($value)
 * @method static Builder|User whereFeaturedImageId($value)
 * @method static Builder|User whereTwoFactorRecoveryCodes($value)
 * @method static Builder|User whereTwoFactorSecret($value)
 * @property-read string $imageurl
 * @property-read MediaCollection|Media[] $media
 * @property-read int|null $media_count
 */
class User extends Authenticatable implements MustVerifyEmail, HasMedia
{
    use Notifiable, HasApiTokens, SoftDeletes, HasTeams, HasProfilePhoto, InteractsWithMedia;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'active', 'activation_token', 'avatar', 'role', 'settings', 'shortcuts', 'expo_push_token', 'allow_push_notifications', 'allow_email_notifications', 'featured_image', 'featured_image_id'
    ];
    protected $appends = ['imageurl', 'avatar_url'];

    protected $table = 'users';



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'activation_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public function getAvatarUrlAttribute(): string
    {
        return Storage::url('avatars/'.$this->id.'/'.$this->avatar);
    }

    public function getImageurlAttribute(): string
    {
        return $this->getFirstMedia('images') ? $this->getFirstMedia('images')->getUrl() : '#';
    }

    public function deals(): HasMany
    {
        return $this->hasMany('App\Models\Deal');
    }

    public function favourites(): BelongsToMany
    {
        return $this->belongsToMany('App\Models\Deal', 'favourites', 'deal_id', 'user_id');
    }

    public function smashes(): BelongsToMany
    {
        return $this->belongsToMany('App\Models\Deal', 'smashes', 'deal_id', 'user_id');
    }

    public function notified(): BelongsToMany
    {
        return $this->belongsToMany('App\Models\Deal', 'notified', 'deal_id', 'user_id');
    }

    public function grabs(): BelongsToMany
    {
        return $this->belongsToMany('App\Models\Deal', 'grabs', 'deal_id', 'user_id');
    }

    public function stores(): HasMany
    {
        return $this->hasMany('App\Models\Store');
    }

    public function notifications(): HasMany
    {
        return $this->hasMany('App\Models\Notification');
    }

    public function images(): MorphMany
    {
        return $this->morphMany('App\Models\Image', 'imageable');
    }
}
